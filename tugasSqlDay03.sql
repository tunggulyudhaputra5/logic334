CREATE DATABASE DB_Sales

use DB_Sales

create table SALESPERSON			
(
ID bigint primary key identity(1,1),
NAME varchar (50) not null,
BOD date not null,
SALARY decimal (18,2) not null
)

INSERT INTO SALESPERSON(NAME,BOD,SALARY)
values 
('Abe','9/11/1988',140000),
('Bob','9/11/1978',44000),
('Chris','9/11/1983',40000),
('Dan','9/11/1980',52000),
('Ken','9/11/1977',115000),
('Joe','9/11/1990',38000)

select * from SALESPERSON


create table ORDERS	
(
id bigint primary key identity (1,1),
ORDER_DATE date not null,
CUST_ID int not null,
SALESPERSON_ID int not null,
AMOUNT decimal (18,2)
)

insert into ORDERS(ORDER_DATE,CUST_ID,SALESPERSON_ID,AMOUNT)
values
('8/2/2020',4,2,540),
('1/22/2021',4,5,1800),
('7/14/2019',9,1,460),
('1/29/2018',7,2,2400),
('2/3/2021',6,4,600),
('3/2/2020',6,4,720),
('5/6/2021',9,4,150)

select * from ORDERS

--a
select name
from SALESPERSON as s
join ORDERS as o
on s.ID = o.SALESPERSON_ID
group by name
having Count(SALESPERSON_ID) > 1

--b
select name
from SALESPERSON as s
join ORDERS as o
on s.ID = o.SALESPERSON_ID
group by name
having sum(AMOUNT) > 1000

--c
select name, DATEDIFF(year,BOD, '2023-11-10') as umur,SALARY,SUM(AMOUNT) as amount
from SALESPERSON as s
join ORDERS as o
on s.ID = o.SALESPERSON_ID
where ORDER_DATE >= '2020' 
group by NAME, DATEDIFF(year,BOD, '2023-11-10'),SALARY
order by umur asc
--umur berdasarkan tanggal penjualan
select name, DATEDIFF(year,BOD, ORDER_DATE) as umur,SALARY,AMOUNT as amount
from SALESPERSON as s
join ORDERS as o
on s.ID = o.SALESPERSON_ID
where ORDER_DATE >= '2020' 
group by NAME, DATEDIFF(year,BOD, ORDER_DATE),SALARY,AMOUNT
order by umur asc

--d
select AVG(AMOUNT) as rata2
from ORDERS
group by SALESPERSON_ID
order by AVG(AMOUNT) desc

select NAME as [Nama Sales],sum(O.AMOUNT) as [Total Amount], avg(O.AMOUNT) as [Rata-rata]
from SALESPERSON S join ORDERS O
on S.ID = O.SALESPERSON_ID
group by S.NAME
order by avg(O.Amount) desc


--e
select name,salary, (SALARY * 0.3) as bonus
from SALESPERSON as s
join ORDERS as o
on s.ID = o.SALESPERSON_ID
group by name, SALARY
having Count(SALESPERSON_ID) > 2

--f
select name, count(o.SALESPERSON_ID) as [Total Order]
from SALESPERSON as s
left join ORDERS as o
on s.ID = o.SALESPERSON_ID
where o.SALESPERSON_ID is null
group by NAME

--g
select name,salary, SALARY - (SALARY * 0.02) as stlterpotong
from SALESPERSON as s
left join ORDERS as o
on s.ID = o.SALESPERSON_ID
where o.SALESPERSON_ID is null
group by name, SALARY

