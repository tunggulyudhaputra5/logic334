--SQL Day 03

--Store Procedure
--Create Store Prosedur (SP)
create procedure sp_retrivemahasiswa 
	--parameter
as 
begin 
	select name, address, email from mahasiswa
end

--run procedure
exec sp_retrivemahasiswa

--edit SP / alter store procedure
alter procedure sp_retrivemahasiswa 
	--parameter
	@id int,
	@name varchar (50)
as 
begin 
	select id,name, address, email from mahasiswa
	where id = @id and name = @name 
end

-- run parameter
exec sp_retrivemahasiswa 1,'Haikal Limanshah'


--FUNTION
create function fn_total_mahasiswa
(
	@id int 
)
returns int
begin
	declare @hasil int 
	select @hasil = count(id) from mahasiswa where id = @id
	return @hasil
end

--run
select dbo.fn_total_mahasiswa(1)

--edit / alter
alter function fn_total_mahasiswa
(
	@id int, @nama varchar (50) 
)
returns int
begin
	declare @hasil int 
	select @hasil = count(id) from mahasiswa where id = @id and name = @nama
	return @hasil
end

--run function
select dbo.fn_total_mahasiswa(1,'Haikal Limanshah')


--coba select function mahasiswa
select mhs.id, mhs.name, dbo.fn_total_mahasiswa(mhs.id,mhs.name)
from mahasiswa mhs
join biodata bio on bio.mahasiswa_id = mhs.id


--truncate
truncate table [nama table]