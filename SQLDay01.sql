--SQLDay01

-- DDL (Data Definition Language)

--CREATE

--Create Database 
CREATE DATABASE db_kampus

--pindah database
USE db_kampus

--drop database(hapus)
DROP DATABASE db_kampus

--create table
CREATE TABLE mahasiswa
(
id bigint primary key identity (1,1),
name varchar (50) not null,
address varchar (50) not null,
email varchar (255) null
)

--create view
create view vw_mahasiswa
as select * from mahasiswa

--select view
select * from vw_mahasiswa

--menampilkan data view
select * from vw_mahasiswa

--ALTER	

--ADD Column
ALTER TABLE mahasiswa
ADD nomor_hp varchar (100) not null


--DROP COLUMN
ALTER TABLE mahasiswa
DROP COLUMN nomor_hp

--alter column
ALTER TABLE mahasiswa
ALTER COLUMN email varchar (100) not null

--DROP(hapus)
--drop database
DROP DATABASE (nama_database)
--drop table
DROP TABLE (nama_table)
--drop view
DROP VIEW (nama_view)


--DML (Data Manipulation Language)


--INSERT
INSERT INTO mahasiswa (name,address,email)
values 
('Tunggul','Semarang','tunggulyudhaputra5@gmail.com')
('haikal','kuningan','haikallimanshah@gmail.com')
('imam','kuningan','imamassidqi@gmail.com'),
('irvan','Jakarta','aliirvan122@gmail.com'),
('rezky','Cengkareng','rezkyfajri08@gmail.com'),
('Tunggul','Semarang','tunggulyudhaputra5@gmail.com'),
('shabrina','Palembang','sabrinaputrif604@gmail.com')
('tes','Palembang','sabrinaputrif604@gmail.com')


--select
select id,name,address,email
from mahasiswa

--update
update mahasiswa
set name = 'Haikal Limanshah'
where id = 1 

--Delete
DELETE mahasiswa
WHERE id = 7 -- atau name = tes

--create table biodata
create table biodata 
(
id bigint primary key identity(1,1),
mahasiswa_id bigint null,
tgl_lahir date null,
gender char(10) null
)

select * from biodata

INSERT INTO biodata (mahasiswa_id, tgl_lahir, gender)
VALUES 
(1, '2020-06-10', 'Pria'),
(2, '2020-07-10', 'Pria'),
(3, '2020-08-10', 'Wanita')

update biodata
set tgl_lahir = '2022-07-08'
where id = 3 

--Join(and,Or, dan Not)
SELECT 
mhs.id as id_mahasiswa, name as nama_mahasiswa, mhs.address as alamat, mhs.email, bio.tgl_lahir as tanggal_lahir,bio.gender as gender
FROM mahasiswa AS mhs
join biodata as bio
on mhs.id = bio.mahasiswa_id
where mhs.id = 2 and mhs.name = 'imam' or not mhs.name ='irvan' and mhs.id = 3 -- tidak sama dengan <>, !=


--ORDER BY(urutan)
select *
From biodata
order by tgl_lahir desc, gender asc -- untuk data tgl_lahir sama maka gender akan dieksekusi jika tidak maka tidak


--TOP(tampilkan)
select top 1 * from biodata order by mahasiswa_id desc -- tampikan 1 baris teratas dan diurutkan mahasiswa_id secara desc

--BETWEEN
select * from biodata where mahasiswa_id between 2 and 3
select * from biodata where tgl_lahir between '2019-01-01' and '2021-01-01'
select * from biodata where mahasiswa_id >= 2 and mahasiswa_id <= 3


--like
select * from mahasiswa
where 
--name like 'i%' --awalan i
--name like '%a' --akhiran a
--name like '%ai%' --mengandung ez
--name like '_a%' --karakter ke 2 = a
--name like 'i_%' --karakter ke 1 = i dan karakter 2 setelahnya harus ada
--name like 'i__%' --karakter ke 1 = a dan karakter 3 setelahnya harus ada
name like 'i%n' --awalan a akhiran o

--Group BY
Select name
From mahasiswa
group by name

--aggregate function (sum,avg,count,min,max)
select sum(id), name, address from mahasiswa group by name,address


update mahasiswa
set address = 'Jogja'
where id = 8

--having
select top 1 sum(id) as jumlah,name
from mahasiswa
--where name = 'tunggul'
where name like 'T%'
group by name 
--having sum(id) >= 13
--order by sum(id) desc