
--SQL Day02

--in 
select * from mahasiswa where name = 'Tunggul' or name = 'irvan'
select * from mahasiswa where name in ('Tunggul', 'irvan')


--add column nlai pada tabel mahasiswa
alter table mahasiswa add nilai int

--update column nilai
update mahasiswa set nilai = 80 where id = 1
update mahasiswa set nilai = 80 where id = 2
update mahasiswa set nilai = 70 where id = 3
update mahasiswa set nilai = 100 where id = 4
update mahasiswa set nilai = 70 where id = 5
update mahasiswa set nilai = 50 where id = 6
update mahasiswa set nilai = 40 where id = 8

select * from mahasiswa

--rata rata
select avg(nilai) from mahasiswa
select SUM(nilai), name from mahasiswa

--join
select * from mahasiswa
select * from biodata

select * 
from mahasiswa mhs
inner join biodata bio on mhs.id = bio.mahasiswa_id

--left join
select * 
from mahasiswa mhs
inner join biodata bio on mhs.id = bio.mahasiswa_id
where bio.id is null

--left join
select * 
from mahasiswa mhs
inner join biodata bio on mhs.id = bio.mahasiswa_id
where mhs.id is null

--disctinct
select Distinct name
from mahasiswa

--SUBSTRING
select SUBSTRING('SQL TUTORIAL',1,3)--ambil kolom 1 dengan 3 karakter

--charindex
SELECT CHARINDEX('t', 'Customer')

--DATALENGTH
SELECT DATALENGTH ('W3SCHOOL.CO.')

--when
select name, nilai,
case 
when nilai >= 80 then 'A'
when nilai >= 60 then 'B'
else 'C'
end
as grade
from mahasiswa

--concat
select concat('SQL','IS','FUN')
select ('SQL' + 'IS' + 'FUN')

select CONCAT('nama : ', name)from mahasiswa

--operator aritmatika
create table penjualan
(
id int primary key identity(1,1),
nama varchar (50) not null,
harga decimal (18,0) not null
)

insert into penjualan (nama, harga)
values
('indomie', 1500),
('Close-up', 3500),
('Pepsoden', 3000),
('brush formula', 2500),
('roti manis', 1000),
('gula', 3500),
('sarden', 4500),
('rokok sampurna', 11000),
('rokok 234', 11000)

select nama, harga, harga*100 as [harga*100] from penjualan


--getdate
select getdate()

--day month year
select Day('2023-11-10')
select Month('2023-11-10')
select year(getdate())

--dateadd
select dateadd(year,1,'2023-11-10')
select DATEADD(month,1,'2023-11-10')
select DATEADD(day,1,'2023-11-10')
select DATEADD(hour,1,'2023-11-10')
select DATEADD(month,1,getdate())

--datediff
select DATEDIFF(year,'2023-11-10', '2024-11-10')
select DATEDIFF(month,'2023-11-10', '2024-11-10')
select DATEDIFF(day,'2023-11-10', dateadd(day,5,getdate()))

--sub query
select * 
from mahasiswa mhs
left join biodata bio on mhs.id = bio.mahasiswa_id
where mhs.nilai = (select max(nilai) from mahasiswa)

create table mahasiswa_new
(
id bigint primary key identity(1,1),
nama varchar (50)not null,
address varchar (50)not null,
email varchar (50)not null
)


insert into mahasiswa_new(nama, address, email)
select name, address, email from mahasiswa

select * from mahasiswa_new


--create indeks
create index index_nameemail on mahasiswa(name,email)

--create unique index
create unique index index_id on mahasiswa(id)

--hapus index
drop index index_id on mahasiswa

--primary key
create table coba (
id int not null,
nama varchar (50) not null
)

alter table coba 
add constraint PK_idnama primary key (id, nama)


insert into coba (id,nama)
values 
(1,'haikal'),
(1,'irvan')

select * from coba

--drop table coba

--drop primary key
alter table coba drop constraint PK_idnama

--delete data
delete coba

--unique key
alter table coba add constraint unique_nama unique(nama)

--foreign key
alter table biodata 
add constraint foreignkey_mahasiswaid foreign key (mahasiswa_id) references mahasiswa(id)

--drop unique key
alter table coba drop constraint unique_nama

--drop foreign key
alter table biodata drop constraint foreignkey_mahasiswaid

--mission complete