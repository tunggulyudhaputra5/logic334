﻿//PadRight();
Padleft();
//Recrusif();

Console.ReadKey();

static void Recrusif()
{
    Console.WriteLine("--Recursif--");
    Console.Write("Masukkan Input Awal : ");
    int start = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Input Akhir : ");
    int end = int.Parse(Console.ReadLine());
    Console.Write("Maskkan tipe(asc/desc) : ");
    string tipe = Console.ReadLine().ToLower();

    //panggil fungsi
    perulangan(start, end, tipe);
}
static int perulangan(int start, int end, string tipe)
{
    if (tipe == "asc")
    {
        if (start == end)
        {
            Console.WriteLine(start);
            return 0;
        }

        Console.WriteLine(start);
        return perulangan(start + 1, end, tipe);
    }
    else
    {
            if (start == end)
            {
                Console.WriteLine(end);
                return 0;
            }
            Console.WriteLine(end);
            return perulangan(start , end - 1, tipe);
        }
    }

static void Padleft()
{
    Console.WriteLine("--Padleft--");
    Console.Write("Masukkan input : ");
    int input = int.Parse(Console.ReadLine());
    Console.Write("Masukkan panjang karakter : ");
    int panjang = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Charnya :");
    char chars = char.Parse(Console.ReadLine());

    //231100001 (dua digit tahun, dua digit bulan, generate length)
    DateTime date = DateTime.Now;

    String code = "";

    code = date.ToString("yyMM") + input.ToString().PadLeft(panjang, chars);

    Console.WriteLine($"Hasil Padleft : {code}");
}
static void PadRight()
{
    Console.WriteLine("--PadRight--");
    Console.Write("Masukkan input : ");
    int input = int.Parse(Console.ReadLine());
    Console.Write("Masukkan panjang karakter : ");
    int panjang = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Charnya :");
    char chars = char.Parse(Console.ReadLine());

    //231100001 (dua digit tahun, dua digit bulan, generate length
    DateTime date = DateTime.Now;

    String code = "";

    code = date.ToString("yyMM") + input.ToString().PadRight(panjang, chars);

    Console.WriteLine($"Hasil Padright : {code}");
}

