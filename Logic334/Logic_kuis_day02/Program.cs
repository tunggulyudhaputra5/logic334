﻿//soal1();
//soal2();
//soal3();
//soal4();
//soal5();
//soal6();



Console.ReadKey();



static void soal1()
{
    Console.WriteLine("--Soal 1--");
//   int r, keliling, luas= 0;
    double phi = 3.14;
    Console.Write("masukkan nilai r : ");
    int r = int.Parse(Console.ReadLine());
    //luas = Math.PI * r * r
    double luas =  phi * r * r;
    Console.WriteLine($"luas = {luas}");
    double keliling = 2 * phi * r ;
    Console.WriteLine($"keliling = {keliling}");

}
static void soal2()
{
    Console.WriteLine("--Soal 2--");
    int luas, keliling = 0;

    Console.Write("masukkan nilai s : ");
    int s = int.Parse(Console.ReadLine());
    
    luas = s * s;
    Console.WriteLine($"luas = {luas}");
    keliling = 4 * s;
    Console.WriteLine($"keliling = {keliling}");

}
static void soal3()
{
    double x, y, hasil = 0;
    Console.WriteLine("--soal3--");
    Console.Write("Masukkan x : ");
    x = int.Parse(Console.ReadLine());
    Console.Write("Masukkan y : ");
    y = int.Parse(Console.ReadLine());

    hasil = x % y;
    if (hasil > 0)
    {
        Console.WriteLine($"sisa = {hasil}");
    }
    else
    {
        Console.WriteLine("habis atau 0");
    }
}
 
static void soal4()
{
    Console.WriteLine("--soal4--");
    int puntung, sisa, hasil, total = 0;
    Console.Write("Masukkan puntung : ");
    puntung = int.Parse(Console.ReadLine());
    int satuan = 8;
    int harga = 500;
    

    sisa = puntung % satuan;
    hasil = puntung / satuan;
    total = hasil * harga;

    Console.WriteLine($"mendapatkan = {hasil} batang dengan sisa = {sisa} puntung");
    Console.WriteLine($"dengan uang yang di dapat = {total} Rupiah");
}

static void soal5()
{
    double x = 0;
    Console.WriteLine("--soal5--");
    Console.Write("Masukkan nilai : ");
    x = int.Parse(Console.ReadLine());

    string z = x >= 80 && x <= 100  ? "A" : x >= 60 && x < 80 ? "B" : x >= 0 && x <60 ? "C" : "Null";
    Console.WriteLine(z);

    /*       if (x >= 80)
           {
               Console.WriteLine("A");
           }
           else if (x >= 60 && x < 80)
           {
               Console.WriteLine("B");
           }
           else
           {
               Console.WriteLine("C");
          }*/
}

static void soal6()
{
    double x = 0;
    Console.WriteLine("--soal6--");
    Console.Write("Masukkan bilangan : ");
    x = int.Parse(Console.ReadLine());

    if (x % 2 == 0)
    {
        Console.WriteLine($"{x} adalah bilangan genap");
    }
    else
    {
        Console.WriteLine($"{x} adalah bilangan ganjil");
    }
}