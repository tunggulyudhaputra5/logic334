﻿using Logic04;

//iniarray();
//akseselemenarray();
//Arr2d();
//inisialisList();
//PanggilClass();
//MengaksesList();
//Insetlist();
//List2Dimensi();
//indeksElemenList();
//inisialisDatetime();
//dateTimeparsing();
//dateTimeproperties();
Timespan();

Console.ReadKey();


static void Timespan()
{
    Console.WriteLine("-- date timespan--");
//    DateTime date1 = DateTime.Now;
    DateTime date1 = new DateTime(2016, 1, 10, 11, 20, 30);
    DateTime date2 = new DateTime(2016, 2, 20, 12, 25, 35);

    TimeSpan interval = date2 - date1;
    Console.WriteLine("Nomor of days : " + interval.Days);
    Console.WriteLine("total Nomor of days : " + interval.TotalDays);
    Console.WriteLine("Nomor of Hours : " + interval.Hours);
    Console.WriteLine("total Nomor of Hours : " + interval.TotalHours);
    Console.WriteLine("Nomor of Minutes : " + interval.Minutes);
    Console.WriteLine("total Nomor of Minutes : " + interval.TotalMinutes);
    Console.WriteLine("Nomor of Second : " + interval.Seconds);
    Console.WriteLine("total Nomor of Second : " + interval.TotalSeconds);
    Console.WriteLine("Nomor of MilliSecond : " + interval.Milliseconds);
    Console.WriteLine("Total Nomor of MilliSecond : " + interval.TotalMilliseconds);
    Console.WriteLine("Nomor of Ticks : "+ interval.Ticks);


}

static void dateTimeproperties()
{
    Console.WriteLine("-- date time Properties--");
    DateTime myDate = new DateTime(2023, 11, 1, 11, 18, 25);
    int myYear = myDate.Year;
    int myMonth = myDate.Month;
    int myDay = myDate.Day;
    int myHour = myDate.Hour;
    int myMinute = myDate.Minute;
    int mySecond = myDate.Second;
    int weekDay = (int)myDate.DayOfWeek;
    string hariString = myDate.DayOfWeek.ToString();
    string hariString2 = myDate.ToString("dddd");

    //Console.WriteLine(myDay + weekDay);
    Console.WriteLine(myYear);
    Console.WriteLine(myMonth);
    Console.WriteLine(myDay);
    Console.WriteLine(myHour);
    Console.WriteLine(myMinute);
    Console.WriteLine(mySecond);
    Console.WriteLine(hariString);
    Console.WriteLine(hariString2);

}

static void dateTimeparsing()
{
    Console.WriteLine("-- parsing date time list--");
    Console.Write(" Masukkan tanggal dd/MM/yyyy : ");
    string dateString = Console.ReadLine();
    try
    {
        DateTime datetime1 = DateTime.ParseExact(dateString, "d/MM/yyyy", null);
        Console.WriteLine(datetime1);
    }
    catch(Exception ex)
    {
        Console.WriteLine("format yang anda masukkan salah");
        Console.WriteLine("Pesan error : "+ ex.Message);
    }
    
}

static void inisialisDatetime()
{
    Console.WriteLine("-- inisialis date time list--");
    //Default date time 1/1/0001 12:00:00 Am
    DateTime dt1 = new DateTime();
    Console.WriteLine(dt1);

    DateTime dtnow = DateTime.Now;
//    Console.WriteLine(dtnow.ToString("dddd/MMMM/yyyy"));// bisa diubah jumlah d, M dan y
    Console.WriteLine(dtnow.ToString("dddd, MMMM, yyyy"));// bisa diubah jumlah d, M dan y

    DateTime dt2 = new DateTime(2023, 11, 1);
    Console.WriteLine(dt2);

    DateTime dt3 = new DateTime(2023, 11, 1, 10, 42, 25);
    Console.WriteLine(dt3);
}

static void indeksElemenList()
{
    Console.WriteLine("-- indeks elemen list--");
    List<string> list = new List<string>();
    list.Add("1");
    list.Add("2");
    list.Add("3");

    //    string item = "3";
    Console.Write("Masukkan data : ");
    string item = Console.ReadLine();

    int indeks = list.IndexOf(item);

    if (indeks != -1)
    {
        Console.WriteLine($"Element {item} is found at indeks {indeks}");
    }
    else
    {
        Console.WriteLine($"Indeks not found{indeks}");
    }
}

static void List2Dimensi()
{
    Console.WriteLine("--List 2 Dimensi--");
    List<List<int>> list = new List<List<int>>()
    {
        new List<int>() { 1, 2, 3 },
        new List<int>() { 4, 5, 6 },
        new List<int>() { 7, 8, 9 }
    };

    //tambah data
    list.Add(new List<int>() { 10, 11, 12 });

    //ubah data
    list[0][1] = 22; //ubah nilai 2 menjadi 22

    //hapus data
    list.RemoveAt(3); //hapus pakai index
    //list.Remove(list[3]); //hapus pakai value

    for (int i = 0; i < list.Count; i++)
    {
        for (int j = 0; j < list[i].Count; j++)
        {
            Console.Write(list[i][j] + "\t");
        }
        Console.WriteLine();
    }
}

static void Insetlist()
{
    Console.WriteLine("-- inset list--");
    List<int> list = new List<int>();
    list.Add(1);
    list.Add(2);
    list.Add(3);

    list.Insert(1, 4);
    //bisa 2x
    list.Insert(3, 4);

    for (int i = 0; i < list.Count; i++)
    {
        Console.WriteLine(list[i]);
    }
}

static void MengaksesList()
{
    Console.WriteLine("--Mengakses element list--");
    List<int> list = new List<int>();
    list.Add(1);
    list.Add(2);
    list.Add(3);

    Console.WriteLine(list[0]);
    Console.WriteLine(list[1]);
    Console.WriteLine(list[2]);
    Console.WriteLine();
    foreach (int item in list)
    {
        Console.WriteLine(item);
    }
    Console.WriteLine();

    for (int i = 0; i < list.Count; i++)
    {
        Console.WriteLine(list[i]);
    }
}

static void PanggilClass()
{
    Console.WriteLine("--PanggilClass--");

    /*//shortcut auto Ctrl + .
    Logic04.Student student = new Logic04.Student();*/

    List<Student> students = new List<Student>()
    {
        new Student() { Id = 1, Name = "Udein Boey"},
        new Student() { Id = 2, Name = "Badrun Oey"},
        new Student() { Id = 3, Name = "Udein Koey"},

    };
    //tambah data dinamis
    Student student = new Student();
    student.Id = 4;
    student.Name = "Joe Koe";
    students.Add(student);

    students.Add(new Student() { Id = 5, Name = "Adoe Doe" });
    Console.WriteLine($"Panjang Data List Student = {students.Count}");

    foreach (Student item  in students)
    {
        Console.WriteLine($"Id :{item.Id}, Name : {item.Name}");
    }
    Console.WriteLine();

    for(int i = 0; i < students.Count; i++)
    {
        Console.WriteLine($"Id :{students[i].Id}, Name : {students[i].Name}");
    }
    

    
}

static void inisialisList()
{
    Console.WriteLine("--Inisialisasi list--");
    List<string> list = new List<string>()
    {
        "Soe Koe",
        "Poe Noe",
        "Sae Koe"
    };
    //tambah data
    list.Add("Ako Oey");

    //ubah data
    list[3] = "Oey Koe";

    //hapus data(Jika indeks tidak ada maka error contoh: RemoveAt berada di bawah remove)
    list.RemoveAt(3);
    list.Remove("Oey Koe");


    Console.WriteLine(string.Join(",",list));
}

static void Arr2d()
{
    Console.WriteLine("--array 2d--");
    int[,] array = new int[3, 3] {
    { 1, 2, 3 },
    { 4, 5, 6 },
    { 7, 8, 9 }
    };
    for (int i = 0; i < array.GetLength(0); i++)
    {
        for (int j = 0; j < array.GetLength(1); j++)
        {
            Console.Write(array[i, j] + " ");
        }
        Console.WriteLine();
    }
}

static void akseselemenarray()
{
    Console.WriteLine("--akses elemen array--");
    int[] intArray = new int[3];
    //isi datanya
    intArray[0] = 1;
    intArray[1] = 4;
    intArray[2] = 5;

    //cetak data
    Console.WriteLine(intArray[0]);
    Console.WriteLine(intArray[1]);
    Console.WriteLine(intArray[2]);

    int[] array = {1, 2, 3, 4};
    for (int i = 0; i < array.Length; i++)
    {
        Console.WriteLine(array[i]);
    }

    string[] stringArr = { "Mikel", "Agus", "Agung" };
    foreach (string i in stringArr)
    {
        Console.WriteLine(i);
    }
}

static void iniarray()
{
    Console.WriteLine("--inisialisasi array--");
    // cara 1
    int [] array = new int[5];
    // cara 2
    int [] array2 = new int[5] {1, 2, 3, 4, 5};
    // cara 3
    int[] array3 = new int[] { 1, 2, 3, 4, 5 };
    // cara 4
    int[] array4 = { 1, 2, 3, 4, 5 };
    // cara 5
    int[] array5;
    array5 = new int[] { 1,2, 3, 4, 5 };
    Console.WriteLine(string.Join(" . ", array));
    Console.WriteLine(string.Join(" . ", array2));
    Console.WriteLine(string.Join(" . ", array3));
    Console.WriteLine(string.Join(" . ", array4));
    Console.WriteLine(string.Join(" . ", array5)); 

}