﻿//While();
//While2();
//DoWhile();
//For();
//Break();
//Continue();
//Forneasted();
//Forneach();
//Length();
//Remove();
//Insert();
//Replace();
//SplitdanJoin();
//Substring();
//kontainsstring();
//Convert();
Convertall();

Console.ReadKey();

static void Convertall()
{
    Console.WriteLine("--Convertall--");
    Console.Write("Masukkan input angka : ");
    string[] input = Console.ReadLine().Split(',');
    
    int sum = 0;
    int[] array = Array.ConvertAll(input, int.Parse);

    foreach (int i in array)
    {
        sum += i;
    }
    Console.WriteLine($"Jumlah = {sum}");
}

static void Convert()
{
    Console.WriteLine("--Convert--");
    Console.Write("Masukkan kalimat : ");
    string kalimat = Console.ReadLine();
    char[] charArr = kalimat.ToCharArray();
    foreach (char c in charArr)
    {
        Console.WriteLine(c);
    }
    Console.WriteLine();

    for (int i = 0; i < charArr.Length; i++)
    {
        Console.WriteLine(charArr[i]);
    }
}

static void kontainsstring()
{
    Console.WriteLine("--kontainstring--");
    Console.Write("Masukkan kata : ");
    string kata = Console.ReadLine();
    Console.Write("Masukkan kontain : ");
    string kontain = Console.ReadLine();

    if (kata.Contains(kontain))
    {
        Console.WriteLine($"kata {kata} mengandung {kontain}");
    }
    else
    {
        Console.WriteLine($"kata {kata} tidak mengandung {kontain}");
    }
}

static void Substring()
{
    Console.WriteLine("--Substring--");
    Console.Write("Masukkan Kode : ");
    string kode = Console.ReadLine();
    Console.Write("Masukkan paramener 1 : ");
    int param1 = int.Parse(Console.ReadLine());
    Console.Write("Masukkan paramener 2 : ");
    int param2 = int.Parse(Console.ReadLine());

    if (param1 == 0)
    {
        Console.WriteLine($"Hasil Substring = {kode.Substring(param1)}");
    }
    else
    {
        Console.WriteLine($"Hasil Substring = {kode.Substring(param2)}");

    }
}

static void SplitdanJoin()
{
    Console.WriteLine("--SplitnJoin--");
    Console.Write("Masukkan kalimat : ");
    string kalimat = Console.ReadLine();
    Console.Write("Masukkan splitnya : ");
    string split = Console.ReadLine();

    string[] katakata = kalimat.Split(split);

    foreach (string kata in katakata)
    {
        Console.WriteLine(kata);
    }
    Console.WriteLine(string.Join(" ", katakata));

    Console.WriteLine();

    int[] deret = {1, 2, 3, 4, 5};
    Console.WriteLine(string.Join(" ", deret));
}

static void Replace()
{
    Console.WriteLine("--Replace--");
    Console.Write("Masukkan kata : ");
    string kata = Console.ReadLine();
    Console.Write("masukan kata yang ingin direplace : ");
    string katalama = Console.ReadLine();
    Console.Write("masukan kata replace : ");
    string katareplace = Console.ReadLine();

    Console.WriteLine($"{kata.Replace(katalama, katareplace)}");
}

static void Insert()
{
    Console.WriteLine("--Insert--");
    Console.Write("Masukkan kata : ");
    string kata = Console.ReadLine();
    Console.Write("masukan indeks insert : ");
    int indeks = int.Parse(Console.ReadLine());
    Console.Write("masukan insert katanya : ");
    string katainsert = Console.ReadLine();

    Console.WriteLine($"{kata.Insert(indeks, katainsert)}");
}


static void Remove()
{
    Console.WriteLine("--Remove--");
    Console.Write("Masukkan kata : ");
    string kata = Console.ReadLine();
    Console.Write("masukan indeks remove : ");
    int indeks = int.Parse(Console.ReadLine());

    Console.WriteLine($"{kata.Remove(indeks)}");
}

static void Length()
{
    Console.WriteLine("--Length--");
    Console.Write("Masukkan kata : ");
    string kata = Console.ReadLine();

    Console.WriteLine($"kata {kata.ToLower()} memiliki jumlah huruf = {kata.Length} huruf");
}

static void Forneach()
{
    Console.WriteLine("--Perualangan Foreach--");
    int[] Array = { 100, 101, 102, 103};
    int sum = 0;

    foreach (int x in Array)
    {
        sum += x;
    }
    Console.WriteLine($"jumlah = {sum}");
    Console.WriteLine();
    for( int i = 0; i < Array.Length; i++)
    {
        sum += Array[i];
    }
    Console.WriteLine($"jumlah = {sum}");
}

static void Forneasted()
{
    Console.WriteLine("--Perualangan Forneasted--");
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
//            Console.Write("((0), (1))" i, j);
            Console.Write($"({i}, {j})");
        }
        Console.Write("\n");
    }
}

static void Continue()
{
    Console.WriteLine("--Perualangan Continue--");
    for (int i = 0; i < 10; i++)
    {
        if (i == 6)
        {
            continue;
        }
        Console.WriteLine(i);
    }
}

static void Break()
{
    Console.WriteLine("--Perualangan Break--");
    for (int i = 0; i < 10; i++)
    {
        if(i == 6)
        {
            break;
        }
        Console.WriteLine(i);
    }
}

static void For()
{
    Console.WriteLine("--Perualangan For--");
    Console.Write("Masukkan nilai : ");
    int nilai = int.Parse(Console.ReadLine());
    for (int i = 0; i < nilai; i++)
    {
        Console.WriteLine(i+ "\t");
        Console.Write("\t");
    }
    Console.WriteLine("\n");
    for (int i = nilai ;i >= 0; i--)
    {
       Console.WriteLine (i);
    }
}

static void DoWhile()
{
    Console.WriteLine("--Perualangan DoWhile--");
    Console.Write("Masukkan nilai : ");
    int nilai = int.Parse(Console.ReadLine());
    string tes = test();
    do
    {
        Console.WriteLine(nilai);
        nilai++;
    } while (nilai < 6);
}

static string test()
{
    return "test";
}

static void While2()
{
    Console.WriteLine("--Perualangan While2--");
    Console.Write("Masukkan nilai : ");
    int nilai = int.Parse(Console.ReadLine());
    bool ulangi = true;

    while (ulangi)
    {
        Console.WriteLine($"Proses ke {nilai}");
        nilai++;

        Console.Write("ulangi proses ? (y/n)");
        string input = Console.ReadLine(); 
        if (input.ToLower() == "n")
        {
            ulangi = false;
        }
    }
}
static void While()
{
    Console.WriteLine("--Perualangan While--");
    Console.Write("Masukkan nilai : ");
    int nilai = int.Parse(Console.ReadLine()); 

    while (nilai < 6)
    {
        Console.WriteLine(nilai);
        nilai++;
    }

}


