﻿// See https://aka.ms/new-console-template for more information
//output
Console.WriteLine("Hello, World!");
Console.WriteLine("Hello, Guys!");
Console.Write("Hello, Guys!");
Console.WriteLine();
Console.Write("Hello, Guys!");

//input
Console.Write("Masukkan nama :");
string nama = Console.ReadLine();
string belakang = "batch334";
int umur = 12;
bool expired = false;
Console.WriteLine(nama);
Console.WriteLine(belakang);

//tipe data yang menyesuaikan
var u = "a";
var y = '2';
var umurr = 12;

//variabel mutable
belakang += " bacth334 ok";
//variabel immutable
const double Phi = 3.14;

//penggabungan
Console.WriteLine("Hai,{0} {1} selamat Datang", nama, belakang);
Console.WriteLine("Hai," + nama + " Selamat Datang"+ belakang);
Console.WriteLine($"Hai,{nama} {belakang} Selamat Datang");

Console.ReadKey();
