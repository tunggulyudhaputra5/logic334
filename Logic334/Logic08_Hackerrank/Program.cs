﻿//solvemefirst();




//hackerrank_warmup

/*static void solvemefirst()
{
    int a = 2; int b = 3;
    int sum = a + b;
    Console.Write(sum) ;
}

static void Comparation()
{
    public static List<int> compareTriplets(List<int> a, List<int> b)
    {
        int aliceScore = 0;
        int bobScore = 0;

        for (int i = 0; i < a.Count; i++)
        {
            if (a[i] == b[i]) continue;
            if (a[i] > b[i])
            {
                aliceScore++;
            }
            else
            {
                bobScore++;
            }
        }

        return new List<int> { aliceScore, bobScore };
    }

}

static void Averybigsum()
{
    public static long aVeryBigSum(List<long> ar)
    {
        return ar.Sum();
    }
}

static void Diagonaldiff()
{
    public static int diagonalDifference(List<List<int>> arr)
    {
        int d1 = 0;
        int d2 = 0;
        int n = arr.Count;

        for (int i = 0; i < n; i++)
        {
            d1 += arr[i][i];
        }

        for (int j = 0; j < n; j++)
        {
            d2 += arr[j][n - 1 - j];
        }

        return Math.Abs(d1 - d2);
    }

}*/


//soal5
/*using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{



    public static void plusMinus(List<int> arr)
    {
        int n = arr.Count;
        int positives = arr.Where(x => x > 0).Count();
        int negatives = arr.Where(x => x < 0).Count();
        int zeroes = arr.Where(x => x == 0).Count();

        float positiveFraction = (float)positives / (float)n;
        float negativeFraction = (float)negatives / (float)n;
        float zeroFraction = (float)zeroes / (float)n;

        Console.WriteLine(positiveFraction);
        Console.WriteLine(negativeFraction);
        Console.WriteLine(zeroFraction);

    }

    class Solution
    {
        public static void Main(string[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine().Trim());

            List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

            Result.plusMinus(arr);
        }
    }
}*/

//soal6
/*using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    *//*
     * Complete the 'staircase' function below.
     *
     * The function accepts INTEGER n as parameter.
     *//*

    public static void staircase(int n)
    {
        for (int i = 1; i <= n; i++)
        {
            Console.WriteLine(new string(' ', n - i) + new string('#', i));
        }
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        int n = Convert.ToInt32(Console.ReadLine().Trim());

        Result.staircase(n);
    }
}*/

//soal7
/*using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    *//*
     * Complete the 'miniMaxSum' function below.
     *
     * The function accepts INTEGER_ARRAY arr as parameter.
     *//*

    public static void miniMaxSum(List<int> arr)
    {
        int x = arr.Max();
        int y = arr.Min();
        long z = 0;
        long w = 0;
        for (int i = 0; i < arr.Count; i++)
        {
            z += arr[i];
            w += arr[i];
        }
        z = z - x;
        w = w - y;
        Console.WriteLine($"{z} {w}");

    }

}

class Solution
{
    public static void Main(string[] args)
    {

        List<int> arr = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arrTemp => Convert.ToInt32(arrTemp)).ToList();

        Result.miniMaxSum(arr);
    }
}*/

//soal8



/*using System.Reflection.Metadata;*/

/*static void soal8()
{

    Console.Write("masukkan tinggi lilin :");
    string[] angka = Console.ReadLine().Split(" ");
    int[] iangka = Array.ConvertAll(angka, int.Parse);
    string maks = Convert.ToString(iangka.Max());
    int jumlah = 0;
    for (int i = 0; i < angka.Length; i++)
    {
        if (maks == angka[i])
        {
            jumlah += 1;
        }
    }
    return jumlah;
}


class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int candlesCount = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> candles = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(candlesTemp => Convert.ToInt32(candlesTemp)).ToList();

        int result = Result.birthdayCakeCandles(candles);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}*/




//soal 10
/*using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;
using System.Diagnostics.Metrics;

class Result
{

    *//*
     * Complete the 'timeConversion' function below.
     *
     * The function is expected to return a STRING.
     * The function accepts STRING s as parameter.
     *//*

    public static string timeConversion(string s)
    {
        DateTime result = Convert.ToDateTime(s);
        return result.ToString("HH:mm:ss");
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string s = Console.ReadLine();

        string result = Result.timeConversion(s);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}*/


//soal2
/*using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    *//*
     * Complete the 'simpleArraySum' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts INTEGER_ARRAY ar as parameter.
     *//*

    public static int simpleArraySum(List<int> ar)
    {

        int sum = 0;
        for (int i = 0; i < ar.Count; i++)
        {
            sum = sum + ar[i];
        }
        Console.WriteLine(sum);

    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int arCount = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> ar = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arTemp => Convert.ToInt32(arTemp)).ToList();

        int result = Result.simpleArraySum(ar);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}*/


/*using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    *//*
     * Complete the 'camelcase' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts STRING s as parameter.
     *//*

    public static int camelcase(string s)
    {
        string input = Console.ReadLine();
        var count = input.Where(c => c >= 'A' && c <= 'Z').Count();
        double total = (double)count + 1;

        return (int)total;

    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string s = Console.ReadLine();

        int result = Result.camelcase(s);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}*/

using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    /*
     * Complete the 'minimumNumber' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts following parameters:
     *  1. INTEGER n
     *  2. STRING password
     */

    public static int minimumNumber(int n, string password)
    {
        // Return the minimum number of characters to make the password strong

        int minchar = 6;
        int wrong = 5;
        int hasil = 0;
        int correct = 0;
        string number = "0123456789";
        string lower_case = password.ToLower();
        string upper_case = password.ToUpper();
        string special_characters = "!@#$%^&*()-+";
        char[] charArr = password.ToCharArray();
        foreach (char c in charArr)
        {
            if (c <= minchar && correct <= 1)
            {
                correct += 1;
            }
            if (number.Contains(c) && correct <= 1)
            {
                correct += 1;
            }
            if (lower_case.Contains(c) && correct <= 1)
            {
                correct += 1;
            }
            if (upper_case.Contains(c) && correct <= 1)
            {
                correct += 1;
            }
            if (special_characters.Contains(c) && correct <= 1)
            {
                correct += 1;
            }

            hasil = wrong - correct;


        }
        return hasil;
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine().Trim());

        string password = Console.ReadLine();

        int answer = Result.minimumNumber(n, password);

        textWriter.WriteLine(answer);

        textWriter.Flush();
        textWriter.Close();
    }
}

