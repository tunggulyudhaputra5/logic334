﻿//soal1();
//soal2();
//soal3();
//soal4();
//soal5();
//soal6();
//soal7();
soal8();

Console.ReadKey();

/*static void soal1()
{
    Console.WriteLine("--soal 1--");
    Console.Write("Masukkan Golongan : ");
    int golongan = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Jam Kerja : ");
    int jamkerja = int.Parse(Console.ReadLine());
    int gol1 = 2000, gol2 = 3000, gol3 = 4000, gol4 = 5000, jammax = 40;
    double lembur1 = ((gol1 * (jamkerja - jammax))*1.5);
    double lembur2 = ((gol2 * (jamkerja - jammax)) * 1.5);
    double lembur3 = ((gol3 * (jamkerja - jammax)) * 1.5);
    double lembur4 = ((gol4 * (jamkerja - jammax)) * 1.5);

    Console.WriteLine("Rician upah sebagai berikut : ");

    if (golongan == 1)
    {
        if (jamkerja <= jammax && jamkerja >= 0) 
        {
            Console.WriteLine($"upah    : {gol1 * jamkerja}");
            Console.WriteLine($"lembur  : 0");
            Console.WriteLine($"total   : {gol1 * jamkerja}");
        }
        else 
        {
            Console.WriteLine($"upah    : {gol1 * (jamkerja - (jamkerja -jammax))}");
            Console.WriteLine($"lembur  : {lembur1}");
            Console.WriteLine($"total   : {lembur1 + (gol1 * (jamkerja - (jamkerja - jammax)))}");
        }      
    }
    else if(golongan == 2)
    {
       if(jamkerja <= jammax && jamkerja >= 0)
        {
            Console.WriteLine($"upah    : {gol2 * jamkerja}");
            Console.WriteLine($"lembur  : 0");
            Console.WriteLine($"total   : {gol2 * jamkerja}");
        }
       else
        {
            Console.WriteLine($"upah    : {gol2 * (jamkerja - (jamkerja - jammax))}");
            Console.WriteLine($"lembur  : {lembur2}");
            Console.WriteLine($"total   : {lembur2 + (gol2 * (jamkerja - (jamkerja - jammax)))}");
        }
    }
    else if(golongan == 3)
    {
        if (jamkerja <= jammax && jamkerja >= 0)
        {
            Console.WriteLine($"upah    : {gol3 * jamkerja}");
            Console.WriteLine($"lembur  : 0");
            Console.WriteLine($"total   : {gol3 * jamkerja}");
        }
        else
        {
            Console.WriteLine($"upah    : {gol3 * (jamkerja - (jamkerja - jammax))}");
            Console.WriteLine($"lembur  : {lembur3}");
            Console.WriteLine($"total   : {lembur3 + (gol3 * (jamkerja - (jamkerja - jammax)))}");
        }
    }
    else if(golongan == 4)
    {
        if (jamkerja <= jammax && jamkerja >= 0)
        {
            Console.WriteLine($"upah    : {gol4 * jamkerja}");
            Console.WriteLine($"lembur  : 0");
            Console.WriteLine($"total   : {gol4 * jamkerja}");
        }
        else
        {
            Console.WriteLine($"upah    : {gol4 * (jamkerja - (jamkerja - jammax))}");
            Console.WriteLine($"lembur  : {lembur4}");
            Console.WriteLine($"total   : {lembur4 + (gol4 * (jamkerja - (jamkerja - jammax)))}");
        }
    }
    else
    {
        Console.WriteLine("Tidak ada upah");
    }
}*/

static void soal1()
{
    Console.WriteLine("--soal 1--");
    Console.Write("Masukkan Golongan : ");
    int golongan = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Jam Kerja : ");
    int jamkerja = int.Parse(Console.ReadLine());

    double upahPerJam = 0;
    double lembur = 0;

    switch (golongan)
    {
        case 1:
            upahPerJam = 2000;
            break;
        case 2:
            upahPerJam = 3000;
            break;
        case 3:
            upahPerJam = 4000;
            break;
        case 4:
            upahPerJam = 5000;
            break;
        default:
            Console.WriteLine("gol tidak valid");
            return;
    }

    int jammax = 40;

    if (upahPerJam > 0)
    {
        if (jamkerja > jammax)
        {
            lembur = (jamkerja - jammax) * upahPerJam * 1.5;
            jamkerja = jammax;
        }

        double totalUpah = jamkerja * upahPerJam;

        Console.WriteLine($"Upah    : {totalUpah}");
        Console.WriteLine($"Lembur  : {lembur}");
        Console.WriteLine($"Total   : {totalUpah + lembur}");
    }
}

static void soal2()
{
    Console.WriteLine("--soal 2--");
    Console.Write("Maskkan teks : ");
    string teks = Console.ReadLine();
    int i = 1;
    string[] katakata = teks.Split(' ');

    foreach(string kata in katakata)
    {
        Console.WriteLine($"Kata {i} {kata}");
        i++;
    }
    Console.WriteLine($"Jumlah kata adalah {katakata.Length}");
  
}

static void soal3()
{
    Console.WriteLine("--soal 3--");
    Console.Write("Masukkan teks: ");
    string teks = Console.ReadLine();
    string[] kataKunci = teks.Split(' ');

    for (int i = 0; i < kataKunci.Length; i++)
    {
        string kata = kataKunci[i];
        int panjangKata = kata.Length;

        if (panjangKata >= 2)
        {
            string hasil = kata[0].ToString();
            for (int j = 1; j < panjangKata - 1; j++)
            {
                hasil += "*";
            }
            hasil += kata[panjangKata - 1];

            kataKunci[i] = hasil;
        }
    }

    string hasilKalimat = string.Join(" ", kataKunci);
    Console.WriteLine(hasilKalimat);
}

static void soal4()
{
    /*Console.WriteLine("--soal 4--");
    Console.Write("Masukkan teks: ");
    string teks = Console.ReadLine();
    string[] kataKunci = teks.Split(' ');

    for (int i = 0; i < kataKunci.Length; i++)
    {
        string kata = kataKunci[i];
        int panjangKata = kata.Length;

        if (panjangKata >= 2)
        {
            string hasil = kata[0].ToString();
            for (int j = 1; j < panjangKata - 1; j++)
            {
                hasil += "*";
            }
            hasil += kata[panjangKata - 1];

            kataKunci[i] = hasil;
        }*/
        Console.WriteLine("--soal 4--");
        Console.Write("Masukkan teks: ");
        string teks = Console.ReadLine();
        string hasilKalimat = string.Join(" ", teks.Split(' ').Select(kata => kata.Length >= 2 ? "*" + kata.Substring(1, kata.Length - 2) + "*" : kata));
        Console.WriteLine(hasilKalimat);
    }

static void soal5()
{
    Console.WriteLine("--soal 5--");
    Console.Write("Masukkan teks: ");
    string teks = Console.ReadLine();
    string hasilKalimat = string.Join(" ", teks.Split(' ').Select(kata => kata.Length >= 2 ? kata.Substring(1) : kata));
    Console.WriteLine(hasilKalimat);
}

static void soal6()
{
    Console.WriteLine("--soal 6--");
    Console.Write("Masukkan panjang data: ");
    int  panjang = int.Parse(Console.ReadLine());
    int angka = 3;
    for (int i = 0; i < panjang; i++)
    {
        if (i % 2 == 1)
        {
            Console.Write("  *  ");
        }
        else
        {
            Console.Write(angka + " ");
            angka = angka * 9;
        }
    }

}
static void soal7()
{
    Console.WriteLine("--soal 7--");

    Console.Write("Masukan jumlah bilangan fibonacci = ");
    int jumlah = Convert.ToInt32(Console.ReadLine());
    int x = 0, y = 1, z = 1;

    for (int i = 0; i < jumlah; i++)
    {
        Console.Write(z + " ");
        z = x + y;
        x = y;
        y = z;
    }
}

/*static void soal8()
{
    Console.WriteLine("--soal 8--");
    Console.Write("Masukan jumlah bilangan n = ");
    int n = Convert.ToInt32(Console.ReadLine());

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if (i == 0)
            {
                Console.Write(j + 1 + " \t");
            }
            else if (i == n - 1)
            {
                Console.Write(n - j + "\t");
            }
            else
            {
                if (j == 0 || j == n - 1)
                {
                    Console.Write("*\t");
                }
                else
                {
                    Console.Write(" \t");
                }
            }
        }
        Console.WriteLine();
    }
}*/

static void soal8()
{
    Console.WriteLine("--soal 8--");
    Console.Write("Masukan jumlah bilangan n = ");
    int n = Convert.ToInt32(Console.ReadLine());
    //int baris = 5, kolom = 10;

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)//j = 0 
        {
            
            if (i == 0 )
            {
                Console.Write(j + 1 + "\t");
            }
            else if (i == n - 1)
            {
                Console.Write(n - j + "\t");
            }
            else if (j == 0 || j == n - 1)
            {
                Console.Write("*\t");
            }
            else 
            {
                Console.Write(" \t");
            }

        }
        Console.WriteLine();
    }
}