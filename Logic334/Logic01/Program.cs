﻿//konversi();
//OperatorAritmatika();
//Modulus();
OperatorPenugasan();
//OperatorPembanding();
//OperatorLogika();
//MethodReturnType();

Console.ReadKey();

static void MethodReturnType()
{
    Console.WriteLine("Cetak  Method Return Type : ");
    Console.Write("masukkan mangga : ");
    int mangga = int.Parse(Console.ReadLine());
    Console.Write("masukkan apel : ");
    int apel = int.Parse(Console.ReadLine());

    int jumlah = hasil(mangga, apel);

    Console.WriteLine($"Hasil mangga + apel = {jumlah}");

}
static int hasil(int mangga, int apel)
{
    int hasil = 0;

    hasil = mangga + apel;

    return hasil;
}

static void OperatorLogika()
{
    Console.WriteLine("==Operator Logika==");
    Console.Write("Enter your Age : ");
    int age = int.Parse(Console.ReadLine());
    Console.Write("Enter your Password : ");
    string password = Console.ReadLine();
    bool isAdult = age > 18;
    bool isPassword = password == "Admin";

    if (isAdult && isPassword)
    {
        Console.WriteLine("Welcome");
    }
    else
    {
        Console.WriteLine("Sorry");
    }
}

static void OperatorPembanding()
{
    int mangga, apel = 0;
    Console.WriteLine("==Operator Perbandingan==");
    Console.Write("Masukkan Mangga : ");
    mangga = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Apel : ");
    apel = int.Parse(Console.ReadLine());
    Console.WriteLine("Hasil Perbandinga : ");
    Console.WriteLine($"Mangga > Apel : {mangga > apel}" );
    Console.WriteLine($"Mangga => Apel : {mangga >= apel}" );
    Console.WriteLine($"Mangga < Apel : {mangga < apel}" );
    Console.WriteLine($"Mangga <= Apel : {mangga <= apel}" );
    Console.WriteLine($"Mangga == Apel : {mangga == apel}" );
    Console.WriteLine($"Mangga != Apel : {mangga != apel}" );
}
static void OperatorPenugasan()
{
    int mangga = 10;
    int apel = 15;

    //isi ulang variabel
    mangga = 15;
    Console.WriteLine("==Operator Penugasan==");
    Console.Write("Masukkan Mangga : ");
    mangga = int.Parse(Console.ReadLine());
    Console.WriteLine($"Mangga : {mangga}");
    Console.Write("Masukkan Apel : ");
    apel = int.Parse(Console.ReadLine());

    apel += 6;
    Console.WriteLine($"Apel : {apel}");

}
static void OperatorAritmatika()
{
    int mangga, apel, hasil = 0;
    Console.WriteLine("==Operator Aritmatika==");
    Console.Write("Masukkan Mangga : ");
    //mangga = Convert.ToInt32(Console.ReadLine());
    mangga = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Apel : ");
    apel = int.Parse(Console.ReadLine());

    hasil = mangga + apel;
    Console.WriteLine($"Hasil Mangga + Apel = {hasil}");
}
static void Modulus()
{
    int mangga, apel,hasil = 0;
    Console.WriteLine("==Operator Aritmatika==");
    Console.Write("Masukkan Mangga : ");
    //mangga = Convert.ToInt32(Console.ReadLine());
    mangga = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Apel : ");
    apel = int.Parse(Console.ReadLine());

    hasil = mangga % apel;
    Console.WriteLine($"Hasil Mangga + Apel = {hasil}");
}
//fungsi atau method

static void konversi()
{
    Console.WriteLine("-konversi-");
    int myInt = 10;
   // int myInt = Convert.ToInt (Console.ReadLine);
    double myDouble = 12.25;
   // string my = myDouble.ToInt();
   bool myBool = false;

    string strMyInt = Convert.ToString(myInt);
    string strMyInt2 = myInt.ToString();
    double doubleMyInt = Convert.ToDouble(myInt);
    int intMyDouble = Convert.ToInt32(myDouble);
    string coba1= Convert.ToString (myDouble);
    double coba2= Convert.ToDouble (myInt);

    Console.WriteLine(coba2);
    Console.WriteLine(coba1);
    Console.WriteLine(strMyInt);
    Console.WriteLine(strMyInt2);
    Console.WriteLine(doubleMyInt);
    Console.WriteLine(myBool.ToString());

}
