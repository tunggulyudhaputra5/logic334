﻿using Logic_Quiz_Day06;
using System;
using System.ComponentModel.Design;
using System.Globalization;

//soal1();
//soal2();
//soal3();
//soal4();
//soal5();
//soal6();
//soal7();
//soal8();
//soal9();
//soal10();
soal11();


Console.ReadKey();



static void soal1()
{
    Console.WriteLine("--soal 1--");
    Console.Write("Masukkan panjang data: ");
    int n = int.Parse(Console.ReadLine());
    Console.Write("Masukkan angka: ");
    int angka = int.Parse(Console.ReadLine());
    Console.Write("Masukkan tambahan: ");
    int tambah = int.Parse(Console.ReadLine());
    
    for (int i = 0; i < n; i++)
    {
        if (i % 2 == 0)
        {
            Console.Write($"-{angka}\t");
            angka = angka + tambah;
        }
        else
        {
            Console.Write(angka + "\t");
            angka = angka + tambah;
        }
    }

}

static void soal2()
{
    Console.WriteLine("--soal 2--");
    Console.Write("Masukkan panjang hh:mm:ss(PM/AM): ");
    string strTime = Console.ReadLine();
    DateTime time;
    if (DateTime.TryParseExact(strTime, "hh:mm:sstt", null,
        DateTimeStyles.None, out time))
    {
        Console.WriteLine($"dirubah {strTime} Menjadi 24 jam = {time:HH\\:mm\\:ss}");
    }
    else
    {
        Console.WriteLine($"tidak dapat dirubah format \"{strTime}\" sebagai DateTime");
    }

}

/*static void soal3()
{
    Console.WriteLine("--soal 3--");
    Console.Write("Masukkan kode baju : ");
    int kode = int.Parse(Console.ReadLine());
    Console.Write("Masukkan ukuran baju : ");
    string ukuran = Console.ReadLine();

    double hargaS = 0;
    double hargaM = 0;
    double hargaR = 0;
    string merkBaju = "";

    switch (kode)
    {
        case 1:
            merkBaju = "IMP";
            hargaS = 200000;
            hargaM = 220000;
            hargaR = 250000;
            break;
        case 2:
            merkBaju = "Prada";
            hargaS = 150000;
            hargaM = 160000;
            hargaR = 170000;
            break;
        case 3:
            merkBaju = "Gucci";
            hargaS = 200000;
            hargaM = 200000;
            hargaR = 200000;
            break;
        default:
            Console.WriteLine("Kode tidak valid");
            return;
    }

    Console.WriteLine($"Merk Baju = {merkBaju}");

    if (ukuran == "S")
    {
        Console.WriteLine($"Harga = {hargaS}");
    }
    else if (ukuran == "M")
    {
        Console.WriteLine($"Harga = {hargaM}");
    }
    else if (ukuran == "R")
    {
        Console.WriteLine($"Harga = {hargaR}");
    }
    else
    {
        Console.WriteLine("Ukuran tidak valid");
    }
}*/

static void soal3()
{
    Console.WriteLine("--soal 3--");
    Console.Write("Masukkan kode baju: ");
    int kode = int.Parse(Console.ReadLine());
    Console.Write("Masukkan ukuran baju: ");
    string ukuran = Console.ReadLine();

    HargaBaju bajuShop = new HargaBaju(kode);
    bajuShop.ShowInfo(ukuran);
}

static void soal4()
{
    Console.WriteLine("--Belanja Andi--");
    Console.Write("Masukkan jumlah uang yang dimiliki Andi: ");
    int uangAndi = int.Parse(Console.ReadLine());

    int[] hargaBaju = { 35, 40, 50, 20 };
    int[] hargaCelana = { 40, 30, 45, 10 };

    int totalBelanjaTerbaik = 0;

    for (int i = 0; i < hargaBaju.Length && i < hargaCelana.Length; i++)
    {
        int totalPasangan = hargaBaju[i] + hargaCelana[i];

        if (totalPasangan <= uangAndi && totalPasangan > totalBelanjaTerbaik)
        {
            totalBelanjaTerbaik = totalPasangan;
        }
    }

    Console.WriteLine($"Total belanja Andi yang mendekati uang Andi: {totalBelanjaTerbaik}");
}


/*static void soal5()
{
    Console.WriteLine("--soal 5--");
    Console.Write("Masukkan angka-angka (pisahkan dengan koma): ");
    string input = Console.ReadLine();
    string[] angkaStr = input.Split(',');

    int[] angka = new int[angkaStr.Length];

    for (int i = 0; i < angkaStr.Length; i++)
    {
        angka[i] = int.Parse(angkaStr[i]);
    }

    Console.Write("Masukkan jumlah Rot: ");
    int rotasi = int.Parse(Console.ReadLine());

    for (int r = 0; r < rotasi; r++)
    {
        if (angka.Length > 0)
        {
            int firstElement = angka[0];

            for (int i = 0; i < angka.Length - 1; i++)
            {
                angka[i] = angka[i + 1];
            }

            angka[angka.Length - 1] = firstElement;
        }
    }

    Console.Write("Hasil pemindahan dengan elemen pertama sebagai elemen terakhir: ");
    for (int i = 0; i < angka.Length; i++)
    {
        Console.Write(angka[i]);
        if (i < angka.Length - 1)
        {
            Console.Write(", ");
        }
    }

    Console.WriteLine();
}*/

static void soal5()
{
    Console.Write("Masukan pemisah : ");
    string split = Console.ReadLine();
    Console.Write("Masukan angka array : ");
    string[] angka = Console.ReadLine().Split(split);
    Console.Write("Masukan rot : ");
    int rot = int.Parse(Console.ReadLine());
    for (int i = 0; i < rot; i++)
    {

        for (int j = i + 1; j < angka.Length; j++)
        {
            Console.Write(angka[j] + ",");
        }
        for (int k = 0; k <= i; k++)
        {
            Console.Write(angka[k]);
            if (k <= i - 1)
            {
                Console.Write(",");
            }
        }
        Console.WriteLine();
    }
}

static void soal6()
{
    Console.WriteLine("--soal 6--");
    Console.Write("Masukkan input: ");
    int arr = int.Parse(Console.ReadLine());  
    Console.Write("Masukkan angka-angka (pisahkan dengan koma): ");
    string input = Console.ReadLine();
    string[] angkaStr = input.Split(',');

    int[] angka = new int[angkaStr.Length];

    for (int i = 0; i < angkaStr.Length; i++)
    {
        angka[i] = int.Parse(angkaStr[i]);
    }

    for (int i = 0; i < angka.Length - 1; i++)
    {
        for (int j = i + 1; j < angka.Length; j++)
        {
            if (angka[i] > angka[j])
            {
                int temp = angka[i];
                angka[i] = angka[j];
                angka[j] = temp;
            }
        }
    }

    Console.Write("Hasil pengurutan: ");
    for (int i = 0; i < angka.Length; i++)
    {
        Console.Write(angka[i]);
        if (i < angka.Length - 1)
        {
            Console.Write(",");
        }
    }

    Console.WriteLine();
}

static void soal7()
{
    Console.WriteLine("--soal 7--");
    Console.Write("Masukkan panjang data: ");
    int n = int.Parse(Console.ReadLine());
    Console.Write("Masukkan angka awal : ");
    int angka = int.Parse(Console.ReadLine());
    Console.Write("Masukkan penambah : ");
    int tambah = int.Parse(Console.ReadLine());

    for (int i = 0; i < n; i++)
    {
        if (i >= 0)
        {
            Console.Write($"{angka} ");
            angka = angka + tambah;
        }
    }
}

static void soal8()
{
    Console.WriteLine("--soal 8--");
    Console.Write("Masukkan panjang data: ");
    int n = int.Parse(Console.ReadLine());
    Console.Write("Masukkan angka awal : ");
    int angka = int.Parse(Console.ReadLine());
    Console.Write("Masukkan penambah : ");
    int tambah = int.Parse(Console.ReadLine());

    for (int i = 0; i < n; i++)
    {
        if (i >= 0)
        {
            Console.Write($"{angka} ");
            angka = angka + tambah;
        }
    }
}

static void soal9()
{
    Console.WriteLine("--soal 9--");
    Console.Write("Masukkan panjang data: ");
    int n = int.Parse(Console.ReadLine());
    Console.Write("Masukkan angka awal : ");
    int angka = int.Parse(Console.ReadLine());
    Console.Write("Masukkan penambah : ");
    int tambah = int.Parse(Console.ReadLine());

    for (int i = 0; i < n; i++)
    {
        if (i >= 0)
        {
            Console.Write($"{angka} ");
            angka = angka + tambah;
        }
    }
}

static void soal10()
{
    Console.WriteLine("--soal 10--");
    Console.Write("Masukkan panjang data: ");
    int n = int.Parse(Console.ReadLine());
    Console.Write("Masukkan angka awal : ");
    int angka = int.Parse(Console.ReadLine());
    Console.Write("Masukkan penambah : ");
    int tambah = int.Parse(Console.ReadLine());

    for (int i = 0; i < n; i++)
    {
        if (i >= 0)
        {
            Console.Write($"{angka} ");
            angka = angka + tambah;
        }
    }
}

static void soal11()
{
    Console.WriteLine("--soal 11--");
    Console.Write("Masukkan panjang data: ");
    int n = int.Parse(Console.ReadLine()); 
    Console.Write("Masukkan angka awal : ");
    int angka = int.Parse(Console.ReadLine());
    Console.Write("Masukkan penambah : ");
    int tambah = int.Parse(Console.ReadLine());

    for (int i = 1; i <= n; i++)
    {
        if (i % 3 != 0)
        {
            Console.Write($"{angka} ");
            angka = angka + tambah;
        }
        else
        {
            Console.Write(" * ");
        }
    }
}