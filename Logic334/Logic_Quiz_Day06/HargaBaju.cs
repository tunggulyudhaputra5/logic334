﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic_Quiz_Day06
{
    public class HargaBaju
    {
        private string merkBaju;
        private double hargaS;
        private double hargaM;
        private double hargaR;

        public HargaBaju(int kode)
        {
            switch (kode)
            {
                case 1:
                    merkBaju = "IMP";
                    hargaS = 200000;
                    hargaM = 220000;
                    hargaR = 250000;
                    break;
                case 2:
                    merkBaju = "Prada";
                    hargaS = 150000;
                    hargaM = 160000;
                    hargaR = 170000;
                    break;
                case 3:
                    merkBaju = "Gucci";
                    hargaS = 200000;
                    hargaM = 200000;
                    hargaR = 200000;
                    break;
                default:
                    merkBaju = "Tidak valid";
                    hargaS = 0;
                    hargaM = 0;
                    hargaR = 0;
                    break;
            }
        }

        public void ShowInfo(string ukuran)
        {
            Console.WriteLine($"Merk Baju = {merkBaju}");

            if (ukuran == "S")
            {
                Console.WriteLine($"Harga = {hargaS.ToString("C2")}");
            }
            else if (ukuran == "M")
            {
                Console.WriteLine($"Harga = {hargaM.ToString("C2")}");
            }
            else if (ukuran == "R")
            {
                Console.WriteLine($"Harga = {hargaR.ToString("C2")}");
            }
            else
            {
                Console.WriteLine("Ukuran tidak valid");
            }
        }
    }
}
