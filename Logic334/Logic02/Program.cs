﻿//IfStatement();
//IfElseStatement();
//IfElseIfStatement();
//IfNeasted();
//Ternary();
//Switch();



Console.ReadKey();

static void Switch()
{
    Console.WriteLine("--Switch--");
    Console.Write("Pilih buah apel,jeruk,pisang : ");
    string pilihan = Console.ReadLine().ToLower();

    switch (pilihan)
    {
        case "apel" :
            Console.WriteLine("anda memilih apel");
            break;
        case "jeruk":
            Console.WriteLine("anda memilih jeruk");
            break;
        case "pisang":
            Console.WriteLine("anda memilih pisang");
            break;
        default:
            Console.WriteLine("anda memilih yang lain");
            break;
    }
}

static void Ternary()
{
    Console.WriteLine("--Ternary--");
    Console.Write("masukkan nilai x : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("masukkan nilai y: ");
    int y = int.Parse(Console.ReadLine());
    if (x >= y)
    {
        Console.WriteLine("X lebih besar dari Y");
    }
    else
    {
        Console.WriteLine("X lebih kecil atau = Y");
    }
    //if else dalam ternary
    string z = x > y ? "X lebih besar dari Y" : x== y ? "X = Y" : "X lebih kecil atau = Y";
    Console.WriteLine(z);

}
static void IfNeasted()
{
    Console.WriteLine("--ifneasted--");
    Console.Write("masukkan nilai  : ");
    int nilai = int.Parse(Console.ReadLine());

    if (nilai >= 50)
    {
        Console.WriteLine("kamu lulus");
        if (nilai == 100)
        {
            Console.WriteLine("keren");
        }
    }
    else
    {
        Console.WriteLine("gagal");
    }
}

static void IfElseIfStatement()
{
    Console.WriteLine("--ifelseif statement--");
    Console.Write("masukkan nilai x : ");
    int x = int.Parse(Console.ReadLine());

    if (x >= 10)
    {
        Console.WriteLine("X lebih besar dari 10");
    }
    else if (x <= 0) 
    {
        Console.WriteLine("X lebih kecil");
    }
    else
    {
        Console.WriteLine("tidak ada");
    }
}

static void IfElseStatement()
{
    Console.WriteLine("--ifelse statement--");
    Console.Write("masukkan nilai x : ");
    int x = int.Parse(Console.ReadLine());

    if (x >= 10)
    {
        Console.WriteLine("X lebih besar dari 10");
    }
    else
    {
        Console.WriteLine("X lebih kecil");
    }
}
static void IfStatement()
{
    Console.WriteLine("--if statement--");
    Console.Write("masukkan nilai x : ");
    int x = int.Parse(Console.ReadLine()); 
    Console.WriteLine("masukkan nilai y: ");
    int y= int.Parse(Console.ReadLine());
    if (x >= 10)
    {
        Console.WriteLine("X lebih besar dari 10");
    }
    if (y <= 10) {
        Console.WriteLine("Y lebih kecil dari 10");
    }
}