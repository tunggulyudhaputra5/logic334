﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    public class persegipanjang
    {
        public double panjang;
        public double lebar;
        public double luaspersegipanjang()
        {
            return panjang * lebar;
        }
        public void tampilkandata()
        {
            Console.WriteLine($"luas = {luaspersegipanjang}");
        }

    }
}
