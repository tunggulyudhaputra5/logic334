﻿
using Logic06;

//Abstrackcalss();
//ObjectClass();
//Constractor();
//Encapsulation();
//Inherritance();
//Overriding();

Console.ReadKey();

static void Overriding()
{
    Console.WriteLine("--overiding--");
    Kucing kucing = new Kucing();
    Paus paus = new Paus();

    Console.WriteLine($"kucing {kucing.pindah()}");
    Console.WriteLine($"paus {paus.pindah()}");
}

static void Inherritance()
{
    Console.WriteLine("--Inheritance--");
    Typemobil typemobil = new Typemobil();

    typemobil.Civic();
}

static void Encapsulation()
{
    Console.WriteLine("--Encapsulation--");
    persegipanjang PP = new persegipanjang();
    PP.panjang = 4.5;
    PP.lebar = 3.5;
    PP.tampilkandata();
}

static void Constractor()
{
    Console.WriteLine("--Constractor--");
    Mobil mobil = new Mobil("B123mx");
    //string platno = mobil.getPlatno();
    string platno = mobil.platno;

    Console.WriteLine($"Mobil dengan nomor polisi {platno}");
}

static void ObjectClass()
{
    Console.WriteLine("--ObjectClass--");
    Mobil mobil = new Mobil("") { nama = "Ferrari", kecepatan = 0, bensin = 10, posisi = 0 };
    //    mobil.nama = "ferarri";
    mobil.percepat();
    mobil.maju();
    mobil.isiBensin(20);
    mobil.utama();
}

static void Abstrackcalss()
{
    Console.WriteLine("--Abstrackcalss--");
    Console.Write("input x : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("input y : ");
    int y = int.Parse(Console.ReadLine());

    testTurunan Calc = new testTurunan();
    int jumlah = Calc.jumlah(x, y);
    int kurang = Calc.kurang(x, y);

    Console.WriteLine($"Hasil {x} + {y} = {jumlah}");
    Console.WriteLine($"Hasil {x} - {y} = {kurang}");
}