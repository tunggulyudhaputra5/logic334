﻿//soal1();
//soal2();
//soal3();
//soal4();
//soal5();
//soal6();
//soal7();
soal8();
//soal9();
//soal10();

Console.ReadKey();

/*static void soal1()
{
    Console.WriteLine("--soal1--");
    Console.Write("nilai : ");
    int x = int.Parse(Console.ReadLine());

    int[] faktorial = new Int32[x];
    int index = 0;
    int hasil = 0;
    for (int i = x; i > 0; i--)
    {

        faktorial[index] = i;
        hasil += faktorial[index];
        index++;

    }
    Console.WriteLine($"{x}! = {hasil}");
    Console.Write(string.Join(" x ", faktorial));
    Console.Write($" = {hasil}");
    
}*/

static void soal1()
{
    Console.WriteLine("Cara Duduk");
    Console.Write("Masukkan jumlah anak (x) : ");
    int x = int.Parse(Console.ReadLine());
    //int[] faktorial = new int[x];
    int jumlah = 1;
    // int index = 0;
    string detail = "";
    for (int i = x; i >= 1; i--)
    {
        //faktorial[index] = i;
        jumlah = jumlah * i;
        detail += detail == "" ? i : " + " + i;
        //  index++;
    }
    //Console.WriteLine($"{x}!  = {string.Join(" x ", faktorial)} = {jumlah}");
    Console.WriteLine($"{x}!  = {detail} = {jumlah}");
    Console.WriteLine($"Ada {jumlah} cara");

}

/*static void soal2()
{
    Console.WriteLine("--soal2--");
    Console.Write("input : ");
    string input = Console.ReadLine().ToUpper();
    char[] input1 = input.ToCharArray();
    int jumlah = 0;
    for (int i = 0; i < input1.Length; i++)
    {
        if (input1[i] != 'S' && input1[i] != 'O')
        {
            jumlah += 1;
        }
    }

    Console.WriteLine("Total Sinyal salah : " + jumlah);

}
*/
static void soal2()
{
    Console.Write("Masukan kode : ");
    string kode = Console.ReadLine().ToUpper();
    Console.Write("Masukan kode sandi yang dicari : ");
    string sos = Console.ReadLine();
    int totalsos = 0;
    string kata = "";
    for (int i = 0; i < kode.Length - 2; i++)
    {
        kata = $"{kode[i]}{kode[i += 1]}{kode[i += 1]}";
        if (kata == sos)
        {
            totalsos++;
        }

    }
    int katasalah = (kode.Length) - totalsos * 3;
    Console.WriteLine($"Total yang salah = {katasalah / 3} ");
    Console.WriteLine($"Total yang benar = {totalsos} ");
}

static void soal3()
{
    Console.WriteLine($"--soal3--");
    Console.Write("Masukkan tanggal pertama (format: dd-MM-yyyy):");
    string date1 = Console.ReadLine();

    Console.Write("Masukkan tanggal kembalinya (format: dd-MM-yyyy):");
    string date2 = Console.ReadLine();

    Console.Write("Masukkan jumlah hari wajib kembali:");
    int dateexp = int.Parse(Console.ReadLine());

    try
    {
        DateTime datetime1 = DateTime.ParseExact(date1, "d-MM-yyyy", null);
        DateTime datetime2 = DateTime.ParseExact(date2, "d-MM-yyyy", null);
        Console.WriteLine(date1);
        Console.WriteLine(date2);

        TimeSpan interval = datetime2 - datetime1;
        Console.WriteLine($"total hari :{interval.Days} hari" );

        int pinalti = interval.Days - dateexp;
        Console.WriteLine($"Total keterlambatan pengembalian adalah = {pinalti} hari");
        int denda = pinalti * 500;
        Console.WriteLine($"Denda yang harus dibayarkan adalah = {denda.ToString("#,#.00")} Rupiah");
    }
    catch (Exception ex)
    {
        Console.WriteLine("format yang anda masukkan salah");
        Console.WriteLine("Pesan error : " + ex.Message);
    }
    
}

/*static void soal4()
{
    Console.WriteLine($"--soal4--");
    Console.Write("Masukkan tanggal awal kelas (format: dd-MM-yyyy): ");
    string startDateStr = Console.ReadLine();

    Console.Write("Masukkan jumlah hari kelas berlangsung: ");
    int totalClassDays = int.Parse(Console.ReadLine());

    Console.Write("Masukkan jumlah hari libur: ");
    int totalHolidays = int.Parse(Console.ReadLine());

    DateTime startDate = DateTime.ParseExact(startDateStr, "dd-MM-yyyy", null);

    DateTime endDate = startDate.AddDays(totalClassDays - 1);

    if (totalHolidays > 0)
    {
        endDate = endDate.AddDays(-totalHolidays);
    }

    while (endDate.DayOfWeek == DayOfWeek.Saturday || endDate.DayOfWeek == DayOfWeek.Sunday)
    {
        endDate = endDate.AddDays(-1);
    }

    DateTime ft1ExamDate = endDate.AddDays(1);

    Console.WriteLine("Tanggal ujian FT1: " + ft1ExamDate.ToString("dd-MM-yyyy"));
}*/

static void soal4()
{
    Console.WriteLine("Soal No 4");
    Console.Write("Masukkan tanggal mulai (dd/mm/yyy) : ");
    string input = Console.ReadLine().ToUpper();
    string[] splitInput = input.Split('/');
    int[] splitInputInt = Array.ConvertAll(splitInput, int.Parse);
    try
    {
        DateTime dt = DateTime.ParseExact(input, "d/MM/yyyy", null);
        DateTime dt1 = new DateTime(splitInputInt[2], splitInputInt[1], splitInputInt[0]);
        Console.Write("Masukkan pelaksanaan kelas : ");
        int ujian = int.Parse(Console.ReadLine());
        Console.Write("Masukkan tgl Hari Libur : ");
        string[] hariLibur = Console.ReadLine().Split(",");
        int[] hariLiburInt = Array.ConvertAll(hariLibur, int.Parse);

        for (int i = 0; i < ujian; i++)
        {
            if ((int)dt1.DayOfWeek == 0 || (int)dt1.DayOfWeek == 6)
            {
                ujian += 1;
            }
            else
            {
                for (int j = 0; j < hariLiburInt.Length; j++)
                {
                    if (hariLiburInt[j] == dt1.Day)
                    {
                        ujian += 1;
                    }
                }
            }
            dt1 = dt1.AddDays(1);
        }
        Console.WriteLine(dt1.ToString("dd/MM/yyyy"));
    }
    catch (Exception e)
    {
        Console.WriteLine(e.Message);
    }

}

static void soal5()
{
    
    Console.Write("masukkan kata : ");
    string kalimat = Console.ReadLine().Trim().ToLower();
    int vokal = 0;
    int nonvokal = 0;

    char[] charArr = kalimat.ToCharArray();
    foreach (char c in charArr)
    {
        if ("aiueo".Contains(c))
        {
            vokal++;
        }
        else
        {
            nonvokal++;
        }
    }
    Console.WriteLine($"Huruf vokal ada : {vokal}");
    Console.WriteLine($"Huruf nonvokal ada : {nonvokal}");

}

static void soal6()
{
    Console.Write("masukkan kata : ");
    string kalimat = Console.ReadLine().ToLower();

    char[] charArr = kalimat.ToCharArray();
    foreach (char c in charArr)
    {
        Console.WriteLine($"***{c}***");
    }
}
/*static void soal6()
{
    Console.WriteLine("--soal6--");
    Console.Write("Masukkan Input kata : ");
    string input = Console.ReadLine();
    for (int i = 0; i < input.Length; i++)
    {
        for (int j = 0; j < input.Length; j++)
        {
            if (j == input.Length / 2)
            {
                if (input.Length % 2 == 0)
                {
                    Console.Write($"{input[i]}*");
                }
                else
                {
                    Console.Write($"{input[i]}");
                }
            }
            else
            {
                Console.Write("*");
            }
        }
        Console.WriteLine();
    }
}*/

static void soal7()
{
    Console.WriteLine("--soal7--");
    Console.Write("Masukkan total menu: ");
    int total = int.Parse( Console.ReadLine() );
    Console.Write("Masukkan harga menu: ");
    string menu = Console.ReadLine();
    Console.Write("Masukkan indeks makanan alergi: ");
    int alergiIndex = int.Parse(Console.ReadLine());
    Console.Write("Masukkan uang elsa: ");
    string uang = Console.ReadLine();

    uang = uang.Replace(".", "");
    menu = menu.Replace(".", "");
    

    string[] hargaStrings = menu.Split(',');

    int totalMakanan = 0;

    for (int i = 0; i < hargaStrings.Length; i++)
    {
        if (int.TryParse(hargaStrings[i], out int harga))
        {
            totalMakanan += harga;
        }
        else
        {
            Console.WriteLine($"Harga tidak valid: {hargaStrings[i]}");
        }
    }
    int alergi = int.Parse(hargaStrings[alergiIndex]);
    int totalSetelahAlergi = totalMakanan - alergi;
    int bayar = totalSetelahAlergi / 2;
    int sisa = Convert.ToInt32(uang) - bayar;

    if (alergiIndex >= 0 && alergiIndex < hargaStrings.Length)
    {
        Console.WriteLine($"Total harga makanan = {totalMakanan}");
        Console.WriteLine($"Total yang elsa bisa makan = {totalSetelahAlergi}");
        Console.WriteLine($"Total yang elsa bayar = {bayar}");
        if (sisa == 0)
        {
            Console.WriteLine("uang pas");
        }
        else if (sisa <= 0)
        {
            Console.WriteLine($"elsa ngutang = {sisa}");
        }
        else
        {
            Console.WriteLine($"sisa uang elsa = {sisa}");
        }
    }
    else
    {
        Console.WriteLine("Indeks alergi tidak valid");
    }

}

static void soal8()
{
    Console.WriteLine("--soal8--");
    Console.Write("masukkan angka : ");
    int angka = int.Parse(Console.ReadLine());

    /*    for (int i = angka; i > 0; i--)
        {
            for (int j = 0; j <= angka; j++)
            {
                if (i <= j)
                {
                    Console.Write("#");
                }
                else
                {
                    Console.Write(" ");
                }
            }
            Console.WriteLine();
        }*/
    for (int i = 1; i <= angka; i++)
    {
        Console.WriteLine(new string(' ', angka - i)+ new string('#', i));
    }
}

static void soal9()
{
    Console.WriteLine("--soal9--");
    Console.WriteLine("Masukkan angka: ");
    string[] angka = Console.ReadLine().Split(" ");
    int k = 1 - 1;
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            Console.Write(angka[k]);
            if(j<3)
            {
                Console.Write("\t");
            }
            k = k+1;
        }
        Console.WriteLine();
    }

    int[] iangka = Array.ConvertAll(angka, int.Parse);
    int dkanan = iangka[0] + iangka[4] + iangka[8];
    int dkiri = iangka[2] + iangka[4] + iangka[6];
    int hasil = dkanan - dkiri;
    Console.WriteLine($"Perbedaan diagonal = {hasil}");

}

static void soal10()
{
    Console.WriteLine("--soal10--");
    Console.Write("masukkan tinggi lilin :");
    string[] angka = Console.ReadLine().Split(" ");
    int[] iangka = Array.ConvertAll(angka, int.Parse);
    string maks = Convert.ToString(iangka.Max());
    int jumlah = 0;
    for (int i = 0; i < angka.Length; i++)
    {
        if(maks == angka[i])
        {
            jumlah += 1;
        }
    }
    Console.WriteLine(jumlah) ;

}
