﻿//soal1();
//soal2();
soal3();
//soal4();
//soal5();
//soal6();
//soal7();
//soal8();

Console.ReadKey();
static void soal1()
{
    double x = 0;
    Console.WriteLine("--soal1--");
    Console.Write("Masukkan nilai : ");
    x = int.Parse(Console.ReadLine());

    string z = x >= 90 && x <= 100 ? "A" : x >= 70 && x <= 89 ? "B" : x >= 50 && x <= 69 ? "C" : x >= 0 && x <= 49 ? "E": "Null";
    Console.WriteLine(z);

    /*       if (x >= 90 && <=100)
           {
               Console.WriteLine("A");
           }
           else if (x >= 80 && x < 89)
           {
               Console.WriteLine("B");
           }
           else
           {
               Console.WriteLine("C");
          }*/
}

static void soal2()
{
    Console.WriteLine("--soal2--");
    Console.Write("Masukkan Pembelian pulsa : ");
    int x = int.Parse(Console.ReadLine());

    /*string z = x >= 10000 && x <= 24999 ? "+80 point"
        : x >= 25000 && x <= 49999 ? "+200 point"
        : x >= 50000 && x <= 99999 ? "+400 point"
        : x >= 100000 ? "+800 point"
        : "Tidak ada point";
    Console.WriteLine(z);*/
    if (x >= 10000 && x <= 24999)
    {
        Console.WriteLine($"Pulsa  : {x}");
        Console.WriteLine($"Point  : +80 point");
    }
    else if(x >= 25000 && x <= 49999)
    {
        Console.WriteLine($"Pulsa  : {x}");
        Console.WriteLine($"Point  : +200 point");
    }
    else if (x >= 50000 && x <= 99999)
    {
        Console.WriteLine($"Pulsa  : {x}");
        Console.WriteLine($"Point  : +400 point");
    }
    else if (x >= 100000)
    {
        Console.WriteLine($"Pulsa  : {x}");
        Console.WriteLine($"Point  : +800 point");
    }
    else
    {
        Console.WriteLine("Tidak ada Bonus Point");
    }
}

static void soal3()
{
    Console.WriteLine("--soal3--");
    Console.Write("Belanja = Rp ");
    int belanja = int.Parse(Console.ReadLine());
    Console.Write("Masukkan KodePromo : ");
    string KodePromo = Console.ReadLine().ToUpper();
    Console.Write("Masukkan jarak : ");
    int jarak = int.Parse(Console.ReadLine());

    double Diskon = 0.4 * belanja;
    double DiskonTotal = 0;
    int MaxJarak = 5, OngkirDefault = 5000, MaxDiskon = 30000, ongkir = 0;
    int OngkirperKM =   OngkirDefault + ((jarak - MaxJarak) * 1000);

    switch (KodePromo)
    {
        case "JKTOVO":
            if (belanja >= 30000)
            {
                DiskonTotal = Math.Min(Diskon, MaxDiskon);
                Console.WriteLine($"Diskon 40% = {DiskonTotal}");
                Console.WriteLine($"Total Belanja = {belanja - DiskonTotal}");
            }
            else
            {
                Console.WriteLine("Diskon 40% belanja gagal kode invalid");
            }
            break;
        default:
            Console.WriteLine("Kode Promo tidak valid");
            break;
    }

    if (jarak > MaxJarak)
    {
        ongkir = OngkirperKM;
        Console.WriteLine($"Ongkir = {ongkir}");
        Console.WriteLine($"Total Pembayaran = {ongkir + (belanja - DiskonTotal)}");
    }
    else
    {
        Console.WriteLine("Jarak default bayar 5000");
        Console.WriteLine($"Total Pembayaran = {OngkirDefault + (belanja - DiskonTotal)}");
    }
}

static void soal4()
{
    Console.WriteLine("--soal4--");
    int Ongkir1 = 5000, Ongkir2 = 10000, Ongkir3 = 20000;
    int Potongan1 = 5000, Potongan2 = 10000;
    Console.Write("Belanja = Rp ");
    int belanja = int.Parse(Console.ReadLine());

    if (belanja <= 49999 && belanja >= 30000)
    {
        Console.WriteLine($"Anda mendapat free ongkir {Ongkir1} dan diskon belanja {Potongan1}");
        Console.WriteLine($"Total Pembayaran = {belanja - Ongkir1 - Potongan1}");
    }
    else if (belanja <= 99999 && belanja >= 50000)
    {
        Console.WriteLine($"Anda mendapat free ongkir {Ongkir2} dan diskon belanja {Potongan2}");
        Console.WriteLine($"Total Pembayaran = {belanja - Ongkir2 - Potongan2}");
    }
    else if (belanja >= 100000)
    {
        Console.WriteLine($"Anda mendapat free ongkir {Ongkir3} dan diskon belanja {Potongan2}");
        Console.WriteLine($"Total Pembayaran = {belanja - Ongkir3 - Potongan2}");
    }
    else
    {
        Console.WriteLine("Sialakan tambah keranjang kuningnya");
    }

}


static void soal5()
{
    Console.WriteLine("--soal5--");
    Console.Write("Masukkan Nama : ");
    string Nama = Console.ReadLine();
    Console.Write("Masukkan Tahun Lahir : ");
    int TahunLahir = int.Parse(Console.ReadLine());

    if (TahunLahir >= 1964 && TahunLahir <= 1944)
    {
        Console.WriteLine($"{Nama} Berdasarkan Tahun Lahir Anda generasi Baby boomer");
    }
    else if (TahunLahir >= 1966 && TahunLahir <= 1979)
    {
        Console.WriteLine($"{Nama} Berdasarkan Tahun Lahir Anda generasi X");
    }
    else if (TahunLahir >= 1980 && TahunLahir <= 1994)
    {
        Console.WriteLine($"{Nama} Berdasarkan Tahun Lahir Anda generasi Y(Millenials)");
    }
    else if (TahunLahir >= 1995 && TahunLahir <= 2015)
    {
        Console.WriteLine($"{Nama} Berdasarkan Tahun Lahir Anda generasi Z");
    }
    else
    {
        Console.WriteLine($"{Nama} Tidaik terdeteksi");
    }
}

static void soal6()
{
    Console.WriteLine("--soal6--");
    Console.Write("Masukkan Nama         : ");
    string nama = Console.ReadLine();
    Console.Write("Masukkan Tunjangan    : ");
    double tunjangan = double.Parse(Console.ReadLine());
    Console.Write("Masukkan Gaji Pokok   : ");
    double gapok = double.Parse(Console.ReadLine());
    Console.Write("Masukkan Banyak Bulan : ");
    int banyakbln = int.Parse(Console.ReadLine());

    double total = gapok + tunjangan;
    double pajak1 = 0.05 * total;
    double pajak2 = 0.1 * total;
    double pajak3 = 0.15 * total;
    double bpjs = 0.03 * total;

    if ( total <= 5000000)
    {
        Console.WriteLine($"Karyawan Atas Nama {nama} slip gaji berikut : ");
        Console.WriteLine($"Pajak           : {pajak1}");
        Console.WriteLine($"BPJS            : {bpjs}");
        Console.WriteLine($"Gaji/Bln        : {total - (pajak1 + bpjs)}");
        Console.WriteLine($"totalGaji/Bln   : {(total - (pajak1 + bpjs)) * banyakbln}");
    }
    else if ( total > 5000000 && total <= 10000000)
    {
        Console.WriteLine($"Karyawan Atas Nama {nama} slip gaji berikut : ");
        Console.WriteLine($"Pajak           : {pajak2}");
        Console.WriteLine($"BPJS            : {bpjs}");
        Console.WriteLine($"Gaji/Bln        : {total - (pajak2 + bpjs)}");
        Console.WriteLine($"totalGaji/Bln   : {(total - (pajak2 + bpjs)) * banyakbln}");
    }
    else if (total > 10000000)
    {
        Console.WriteLine($"Karyawan Atas Nama {nama} slip gaji berikut : ");
        Console.WriteLine($"Pajak           : {pajak3}");
        Console.WriteLine($"BPJS            : {bpjs}");
        Console.WriteLine($"Gaji/Bln        : {total - (pajak3 + bpjs)}");
        Console.WriteLine($"totalGaji/Bln   : {(total - (pajak3 + bpjs)) * banyakbln}");
    }
    else
    {
        Console.WriteLine("error");
    }

}

static void soal7()
{
    Console.WriteLine("--soal7--");
    Console.Write("Berat =  ");
    double berat = double.Parse(Console.ReadLine());
    Console.Write("tinggi =  ");
    double tinggi = double.Parse(Console.ReadLine());
    double BMI = (berat / tinggi / tinggi) * 10000;
    BMI = Math.Round(BMI, 2);
    Console.WriteLine($"Nilai BMI anda Adalah = {BMI}");

    if (BMI < 18.5)
    {
        Console.WriteLine($"Anda Terlalu Kurus");
    }
    else if (BMI >= 18.5 && BMI < 25)
    {       
        Console.WriteLine($"Anda Berbadan Langsing/Sehat");
    }
    else if (BMI >= 25)
    {
        Console.WriteLine($"Anda Tergolong Gemuk");
    }
    else
    {
        Console.WriteLine("Null");
    }
}

static void soal8()
{
    Console.WriteLine("--soal8--");
    Console.Write("Nilai MTK =  ");
    double Mtk = double.Parse(Console.ReadLine());
    Console.Write("Nilai Fisika =  ");
    double Fisika = double.Parse(Console.ReadLine());
    Console.Write("Nilai Kimia =  ");
    double Kimia = double.Parse(Console.ReadLine());

    double rata2 = (Mtk + Fisika + Kimia) / 3;
    rata2 = Math.Round(rata2, 2);

    if (rata2 < 50)
    {
        Console.WriteLine($"nilai rata-rata kamu :{rata2}");
        Console.WriteLine("maaf kamu gagal");
    }
    else 
    {
        Console.WriteLine($"nilai rata-rata kamu :{rata2}");
        Console.WriteLine("Selamat Kamu Berhasil");
        Console.WriteLine("Kamu Hebat");
    }

}

