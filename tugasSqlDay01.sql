--tugas sql day01

CREATE DATABASE 
DBPenerbit


--tabelpengarang
CREATE TABLE
tabelPengarang
(
ID bigint PRIMARY KEY IDENTITY(1,1),
Kd_Pengarang varchar (7) not null,
Nama varchar (30) not null,
Alamat varchar (80) not null,
kota varchar (15) not null,
Kelamin varchar (1) not null
)

INSERT INTO tabelPengarang(Kd_Pengarang,Nama,Alamat, kota, Kelamin)
values 
('P0001','Ashadi','Jl. Beo 25', 'Yogya','P'),
('P0002','Rian','Jl. Solo 123', 'Yogya','P'),
('P0003','Suwadi','Jl. Semangka 13', 'Bandung','P'),
('P0004','Siti','Jl. Durian 15', 'Solo','W'),
('P0005','Amir','Jl. Gajah 33', 'Kudus','P'),
('P0006','Suparman','Jl. Harimau 25', 'Jakarta','P'),
('P0007','Jaja','Jl. Singa 7', 'Bandung','P'),
('P0008','Saman','Jl. Naga 12', 'Yogya','P'),
('P0009','Anwar','Jl. Tidar 6A', 'Magelang','P'),
('P0010','Fatmawati','Jl. Renjana 4', 'Bogor','W')

select * FROM tabelPengarang

--tblgaji
CREATE TABLE
tabelgaji
(
ID bigint PRIMARY KEY IDENTITY(1,1),
Kd_Pengarang varchar (7) not null,
Nama varchar (30) not null,
gaji decimal (18,4) not null
)

INSERT INTO tabelgaji(Kd_Pengarang,Nama,gaji)
values 

('P0002','Rian','600000'),
('P0005','Amir','700000'),
('P0004','Siti','500000'),
('P0003','Suwadi','1000000'),
('P0010','Fatmawati','600000'),
('P0008','Saman','750000')

select * FROM tabelgaji

--soal 1
SELECT COUNT(id) FROM tabelPengarang as jumlahpengarang;

--soal 2
select Count(kelamin) as pria, Kelamin
from tabelPengarang
where Kelamin like 'P'
group by Kelamin 

select Count(kelamin) as Wanita,kelamin
from tabelPengarang
--where Kelamin like 'W'
group by Kelamin 


--soal 3
select Count(kota) as totalkota, kota
from tabelPengarang
where not kota = 'magelang' and not kota = 'solo'
group by kota 
order by totalkota desc

--soal 4
select Count(kota) as totalkota, kota
from tabelPengarang
group by kota 
having Count(kota) > 1

--soal 5
select MAX(Kd_Pengarang) as terbesar, MIN(Kd_Pengarang) as terkecil 
from tabelPengarang

--soal 6
select MAX(gaji) as terbesar, MIN(gaji) as terkecil 
from tabelgaji

--soal 7
select gaji
from tabelgaji
--group by gaji 
where gaji > 600000

--soal 8
select sum(gaji) as jumlah
from tabelgaji

--soal 9
SELECT 
tblpengarang.kota as kota, tblgaji.gaji as gaji
FROM tabelPengarang AS tblpengarang
join tabelgaji as tblgaji
on tblpengarang.ID = tblgaji.id


--soal 10
select *
from tabelPengarang
where Kd_Pengarang between 'P0003' and 'P0006'

--soal 11
select *
from tabelPengarang
--where kota = 'Yogya' OR kota = 'Solo' OR kota = 'Magelang'
where kota in ('Magelang', 'Yogya', 'Solo')

--soal 12
select *
from tabelPengarang
where NOT kota =  'Yogya' 

--soal 13
--a
select *
from tabelPengarang
where Nama like 'A%'
--b
select *
from tabelPengarang
where Nama like '%i'
--c
select *
from tabelPengarang
where Nama like '__a%'
--d
select *
from tabelPengarang
where Nama NOT like '%n'

--soal 14
SELECT 
*
FROM tabelPengarang AS tblpengarang
join tabelgaji as tblgaji
on tblpengarang.Kd_Pengarang = tblgaji.Kd_Pengarang

--soal 15
SELECT 
tblpengarang.kota as kota, tblgaji.gaji as gaji
FROM tabelPengarang AS tblpengarang
join tabelgaji as tblgaji
on tblpengarang.ID = tblgaji.id
where gaji < 1000000

--soal 16
ALTER TABLE  tabelPengarang 
ALTER COLUMN kelamin varchar (12) not null

--soal 17
ALTER TABLE tabelPengarang
ADD Gelar varchar (12) null

--soal 18
update tabelPengarang
set Alamat = 'Jl. Cendrawasih 65', kota = 'Pekanbaru'
where id = 2

--soal 19
create view vwPengarang
AS select tblPengarang.Kd_Pengarang, tblPengarang.Nama, tblPengarang.kota, tblgaji.Gaji 
from tabelPengarang as  tblPengarang
join tabelgaji as tblgaji
on tblPengarang.Kd_Pengarang = tblgaji.Kd_Pengarang

select * from vwPengarang
