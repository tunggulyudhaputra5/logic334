--TUGAS SQL DAY 02

CREATE DATABASE DB_Entertainer

USE DB_Entertainer

create table artis
( 
id bigint primary key identity(1,1),
Kd_artis varchar (100) not null,
nm_artis varchar (100) not null,
jk varchar (100) not null,
bayaran int  not null,
award int not null,
negara varchar (100) not null
)
--ALTER TABLE  artis 
--ALTER COLUMN bayaran bigint not null

INSERT INTO artis (Kd_artis, nm_artis, jk, bayaran, award, negara)
Values
('A001', 'ROBERT DOWNEY JR', 'PRIA', '3000000000', '2', 'AS'),
('A002', 'ANGELINA JOLIE', 'WANITA', '700000000', '1', 'AS'),
('A003', 'JAKIE CHAN', 'PRIA', '200000000', '7', 'HK'),
('A004', 'JOE TASLIM', 'PRIA', '350000000', '1', 'ID'),
('A005', 'CHELSEA ISLAN', 'WANITA', '30000000', '0', 'ID')

create table film
(
id bigint primary key identity (1,1),
Kd_film varchar (10) not null,
nm_film varchar (55) not null,
genre varchar (55) not null,
artis varchar (55) not null,
produser varchar (55) not null,
pendapatan bigint not null,
nominasi int not null
)

select * from film

INSERT INTO film (Kd_film, nm_film, genre, artis, produser,pendapatan,nominasi)
values
('F001', 'IRONMAN'				, 'G001', 'A001', 'PD01','2000000000','3'),
('F002', 'IRONMAN 2'			, 'G001', 'A001', 'PD01','1800000000','2'),
('F003', 'IRONMAN 3'			, 'G001', 'A001', 'PD01','1200000000','0'),
('F004', 'AVENGER CIVIL WAR'	, 'G001', 'A001', 'PD01','2000000000','1'),
('F005', 'SPIDERMAN HOMECOMING'	, 'G001', 'A001', 'PD01','1300000000','0'),
('F006', 'THE RAID'				, 'G001', 'A004', 'PD03','800000000','5'),
('F007', 'FAST & FURIOUS'		, 'G001', 'A004', 'PD05','830000000','2'),
('F008', 'HABIBIE DAN AINUN'	, 'G004', 'A005', 'PD03','670000000','4'),
('F009', 'POLICE STORY'			, 'G001', 'A003', 'PD02','700000000','3'),
('F010', 'POLICE STORY 2'		, 'G001', 'A003', 'PD02','710000000','1'),
('F011', 'POLICE STORY 3'		, 'G001', 'A003', 'PD02','615000000','0'),
('F012', 'RUSH HOUR'			, 'G003', 'A003', 'PD05','695000000','2'),
('F013', 'KUNGFU PANDA'			, 'G003', 'A003', 'PD05','923000000','5')

--ALTER TABLE  film 
--ALTER COLUMN artis varchar (100) not null


CREATE TABLE produser 
(
id bigint primary key identity(1,1),
Kd_produser varchar (50) not null,
nm_produser varchar (50) not null,
international varchar (50) not null
)

INSERT INTO produser(Kd_produser,nm_produser,international)
values
('PD01', 'MARVEL', 'YA'),
('PD02', 'HONGKONG CINEMA', 'YA'),
('PD03', 'RAPI FILM', 'YA'),
('PD04', 'PARKIT', 'YA'),
('PD05', 'PARAMOUNT PICTURE', 'YA')

SELECT * FROM produser

CREATE TABLE negara
(
id bigint primary key identity (1,1),
Kd_negara varchar (100) not null,
nm_negara varchar (100) not null
)
--ALTER TABLE  negara 
--ALTER COLUMN nm_negara varchar (100)  not null

INSERT INTO negara (Kd_negara, nm_negara)
values
('AS', 'AMERIKA SERIKAT'),
('HK', 'HONGKONG'),
('ID', 'INDONESIA'),
('IN', 'INDIA')

SELECT * FROM negara

CREATE TABLE genre
(
id bigint primary key identity (1,1),
Kd_genre varchar (50) not null,
nm_genre varchar (50) not null
)

INSERT INTO genre (Kd_genre, nm_genre)
values 
('G001', 'ACTION'),
('G002', 'HORROR'),
('G003', 'COMEDY'),
('G004', 'DRAMA'),
('G005', 'THRILLER'),
('G006', 'FICTION')

SELECT * FROM genre

--SOAL 1
Select f.nm_produser, sum(pendapatan) as pendapatan
From film
join produser as f
on produser = f.Kd_produser
where f.nm_produser = 'marvel'
group by nm_produser

--soal 2
select nm_film, nominasi
from film
where nominasi = 0

--soal3
select nm_film
from film
--where nm_film like 'p%'
where substring(nm_film, 1, 1) = 'P'

--soal 4
select nm_film
from film
--where nm_film like '%y'
where SUBSTRING (nm_film, DATALENGTH(nm_film), 1) ='y'


--soal 5
select nm_film
from film
where nm_film like '%d%'

--soal 6
Select f.nm_film as nm_film, a.nm_artis as nm_artis
From artis as a
join film as f
on a.Kd_artis = f.artis
where nm_film like 'iron%' or nm_artis like 'jakie%'

--soal 7
Select f.nm_film as nm_film, a.negara as negara
From artis as a
join film as f
on a.Kd_artis = f.artis
where negara = 'HK'

--soal 8
Select f.nm_film as nm_film, n.nm_negara as nm_negara
From artis as a
join film as f
on a.Kd_artis = f.artis
join negara as n
on n.Kd_negara = a.negara
--where nm_negara not like '%O%'
where nm_film like '___n%'


--soal 9
--cara 1
SELECT nm_artis
FROM artis
WHERE Kd_artis NOT IN
    (SELECT artis 
     FROM film)
--cara 2
select nm_artis
from artis a
left join film f
on a.Kd_artis = f.artis
where artis is null

--soal 10
Select a.nm_artis as nm_artis, g.nm_genre as nm_genre
From artis as a
join film as f
on a.Kd_artis = f.artis
join genre as g
on g.Kd_genre = f.genre
where nm_genre = 'drama'

--soal 11
--Select distinct a.nm_artis as nm_artis, g.nm_genre as nm_genre
Select a.nm_artis as nm_artis, g.nm_genre as nm_genre
From artis as a
join film as f
on a.Kd_artis = f.artis
join genre as g
on g.Kd_genre = f.genre
where nm_genre = 'action'
group by nm_genre, nm_artis

--soal12
SELECT n.Kd_negara, n.nm_negara, COUNT(f.nm_film) as jumlah_film
FROM negara n
LEFT JOIN artis a 
ON a.negara = n.Kd_negara
LEFT JOIN film f 
ON f.artis = a.Kd_artis
GROUP BY n.Kd_negara, n.nm_negara
ORDER BY n.nm_negara;

--soal13
select nm_film
from film f
join produser p
on f.produser = p.Kd_produser
where international = 'YA'

--soal 14
select nm_produser, count(f.produser) as jumlah_film
from film f
right join produser p
on f.produser = p.Kd_produser
where nm_produser = 'marvel'
group by nm_produser
having count(f.produser) >= 3 --and nm_produser = 'marvel'



